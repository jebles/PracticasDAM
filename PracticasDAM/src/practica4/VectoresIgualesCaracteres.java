package practica4;

/**
 * VectoresIgualesCaracteres.java		24 nov. 2018		practica4 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * método que se llame compararVectoresChar() que recibe dos vectores de caracteres. 
 * Devuelve 0 si son iguales, 1 si el primero es mayor y -1 si el primero es menos.
 * Prueba el método pedido desde main().
 * El orden alfabético de vectores de caracteres requiere la comparación uno a uno de sus caracteres, 
 * comenzando desde el extremo izquierdo. 
 * El orden alfabético corresponde al orden numérico creciente del código del carácter.
 */
public class VectoresIgualesCaracteres {

	public static void main(String[] args) {

		//instancia tres arrays

		char[] array_A = {'a','b','c','d'} ;
		char[] array_B = {'a','b','c','d'};
		char[] array_C = {'a','b','c','a'};
		char[] array_D = {'a','d','c','d'};		

		//hace las comparaciones entre varios de ellos con el método pedido y devuelve true ii son iguales o false si no

		System.out.println(compararVectoresChar(array_A, array_B));
		System.out.println(compararVectoresChar(array_A, array_C));
		System.out.println(compararVectoresChar(array_A, array_D));

	}

	private static int compararVectoresChar(char[] V1, char[] V2) {
		int resultado_comparacion = 0;
		
		String cadenaChar1 = "";
		String cadenaChar2 = "";

		//si son del mismo tamanyo, comparar valores

		if (V1.length == V2.length) {
			//System.out.println("mismo tamanyo");

			cadenaChar1 = recorrerYalmacenar(V1);
			System.out.println(cadenaChar1);
			cadenaChar2 = recorrerYalmacenar(V2);
			System.out.println(cadenaChar2);
			
			resultado_comparacion = cadenaChar1.compareTo(cadenaChar2);
			if ( resultado_comparacion > 0 ) {
				resultado_comparacion = 1;
			}
			if ( resultado_comparacion < 0) {
				resultado_comparacion = -1;
			} 			

		}
		return resultado_comparacion;

	}

	private static String recorrerYalmacenar(char[] V1) {
		String cadenaResultado = ""; 
		for (char elementoArray : V1 ) {			
			cadenaResultado += elementoArray;			
		}
		return cadenaResultado ;
	}
}