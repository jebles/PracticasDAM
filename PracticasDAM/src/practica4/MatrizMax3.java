package practica4;

/**
 * MatrizMax3.java		24 dic. 2018		practica4 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * metodo que busca una submatriz de dos dimensiones con la suma maxima dento de otra cuadrada tambien. 
 */
public class MatrizMax3 {

	public static void main(String[] args) {

		// Declaración de la matriz.
		int[][] matriz = {
				{ 0, 2, 4, 1, 9, 3, 7 },
				{ 7, 1, 3, 9, 2, 9, 2 },
				{ 1, 3, 9, 8, 9, 6, 1 },
				{ 4, 6, 2, 9, 1, 0, 2 },
				{ 5, 4, 3, 1, 1, 2, 8 }
		};

		// Buscar la submatriz de suma máxima de tamaño de 3 x 3.
		int maxSuma = Integer.MIN_VALUE;
		int mejorFila = 0;
		int mejorColum = 0;
		
		for (int fila = 0; fila < matriz.length - 2; fila++) /*recorre filas*/{
			for (int colum = 0; colum < matriz[0].length - 2; colum ++) /*recorre colunmas*/{
				
				/* en el int suma, caclula el total de cada valor con sus adyacientes a 2 de distancia*/
				int suma = matriz[fila][colum] +  
						matriz[fila][colum + 1] +
						matriz[fila + 1][colum]+
						matriz[fila + 1][colum + 1] +
								matriz[fila][colum + 2] +
								matriz[fila + 2][colum]+
								matriz[fila + 2][colum + 2]; 
				/* va acumulando la suma mayor en maxSuma */
				if (suma > maxSuma) {
					maxSuma = suma;
					/*recoge los indices de fila y de columna que le corresponden a la suma mayor*/
					mejorFila = fila; 
					mejorColum = colum;
				}
			}
		}

		// Muestra resultados.
		System.out.println("la submatriz máxima es: ");
		System.out.printf("  %d %d %d%n",
				matriz[mejorFila][mejorColum],
				matriz[mejorFila][mejorColum + 1],
				matriz[mejorFila][mejorColum + 2]);		

		System.out.printf("  %d %d %d%n",
				matriz[mejorFila + 1][mejorColum],
				matriz[mejorFila + 1][mejorColum + 1],
				matriz[mejorFila + 1][mejorColum + 2]);
		
		System.out.printf("  %d %d %d%n",
				matriz[mejorFila + 2][mejorColum],
				matriz[mejorFila + 2][mejorColum + 1],
				matriz[mejorFila + 2][mejorColum + 2]);

		System.out.printf("la suma máxima es: %d%n", maxSuma);


	}

}
