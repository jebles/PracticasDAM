package practica4;

/**
 * VectoresIguales.java		24 nov. 2018		practica4 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * método que se llama vectoresIntIguales() que recibe dos vectores de enteros. 
 * Devuelve true si son iguales.
 * Dos vectores son iguales si tienen la misma longitud y los valores, elemento a elemento, son iguales. 
 * La segunda condición se puede comprobar con un bucle.
 */
public class VectoresIguales {

	public static void main(String[] args) {
		
		//instancia tres arrays
		
		int[] array_A = {1,2,3,4} ;
		int[] array_B = {1,2,3,4};
		int[] array_C = {1,2,3};
		int[] array_D = {1,2,4,4};		
		
		//hace las comparaciones entre varios de ellos con el método pedido y devuelve true ii son iguales o false si no
		System.out.println(vectoresIntIguales(array_A, array_B));
		System.out.println(vectoresIntIguales(array_A, array_C));
		System.out.println(vectoresIntIguales(array_A, array_D));

	}
	
	private static boolean vectoresIntIguales(int[] V1, int[] V2) {
		boolean igualdad = false;

		if ((V1.length == (V2.length))) {
			//si son del mismo tamanyo, comparar valores

			for (int i = 0; i < V1.length ; i++) { // comprueba cada valor dentro de los dos arrays

				if (V1[i] == V2[i]) { // en vaso que sean iguales					
					//System.out.println(V1[i] + " = " + V2[i]);	// devuelve true si coinciden uno a uno
					igualdad = true;

				} else {
					//System.out.println(V1[i] + " != " + V2[i]); // en caso que alguno no coincida, devuelve false	
					igualdad = false;
					break; // en caso de que una de las comparaciones, sale del bucle y queda false
				}
			}
		}
		return igualdad;
	}
}