package practica4;

/**
 * busquedaBinaria.java		28 dic. 2018		practica4 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * Escribe un método que se llame buscarNumero() que recibe un vector ordenado de enteros y un valor. 
 * Devuelve el índice de la posición de ese valor dentro del array recibido utilizando una búsqueda binaria.
 */
public class busquedaBinaria {

	public static void main(String[] args) {
							   // 0, 1, 2, 3, 4, 5
		int [] matrizOrdenada = { 3, 4, 5, 6, 7, 8};
		int num = 8;
		System.out.println(buscarNumero(matrizOrdenada,num));
	}
	private static int buscarNumero(int[] v, int num) {
		int indice = 0;
		
		for (int i = 0; i < v.length; i++) {
			if (v[i] == num) {
				indice = i;
				break;
			}
		}
		return indice;
		
	}

}
