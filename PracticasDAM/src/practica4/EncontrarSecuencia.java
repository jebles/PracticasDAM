package practica4;


/**
 * Secuencia.java		27 nov. 2018		practica4 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * método buscarSecuenciaInt() que recibe dos vectores de enteros, 
 * el segundo es una secuencia de valores a buscar en el primer vector. 
 * Devuelve el índice donde empieza la primera ocurrencia de la secuencia buscada.
 * Por ejemplo: {4, 3, 1, 4, 2, 5, 8} y {4, 2, 5} devolvería 3.
 * Hay que recorrer el vector secuencialmente comprobando coincidencia con el primer elemento de la secuencia buscada. 
 * Después hay que comprobarla completamente con otro bucle. 
 * Si hay coincidencia completa termina sin llegar al final del recorrido.
 */
public class EncontrarSecuencia {


	public static void main(String[] args) {	
					//   0  1  2  3  4  5  6  7  8  9  10  11  12
		int VectorA[] = {4, 1, 7, 1, 2, 7, 6, 6, 1, 1, 7,  4 , 6};
		int VectorB[] = {4, 1, 2, 2, 6, 8 , 8, 6, 6};

		System.out.println("El indice buscado es " + buscarSecuenciaInt(VectorA,VectorB));		
	}

	private static int buscarSecuenciaInt(int[] A, int[] B) {

		int indice = -1;
		boolean encontrado = false;

		for (int i = 0; i < A.length; i++) { //recorre el vector A con el contador i

			for (int j = 0; j < B.length; j++) { // recorre el vector B con el contador j

				if (A[i] == B[0] && encontrado == false) {	//si localiza el valor del primer indice (cero) de B en A, almacena el valor si no ha enccontrado ya el vector 
					System.out.println("localizado B[cero] = A[" + i +"]");
					indice = i; //guarda el indice donde comienza la coincidenca de a	
					encontrado = buscarRestoVectorB(i, j, B, A);
				} 
			} 
		}
		if (encontrado == false) {
			indice = -1;
			System.out.println("la secuencia del vector B no ha sido encontrada en el vector A");
		}		
		return indice;
	}

	private static boolean buscarRestoVectorB(int i, int j, int[] B, int[] A) {
		int contador = 0;
		boolean resultado = false;
		while (A[i] == B[j] && resultado == false) { //si no lo ha encontrado, va comparando incrementalmente hasta que llega al tamaño de B o
			contador++;			
			System.out.println("localizado A["+i+"] = B[" + j + "]\n contamos un acierto: "
					+ " el contador va por " + contador);			

			if (contador == B.length) {
				System.out.println("Vector encontrado. Saliendo...");
				resultado = true;
				break;
			}
			i++;
			j++;
		}
		return resultado;		
	}
}	
