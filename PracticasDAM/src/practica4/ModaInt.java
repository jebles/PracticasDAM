package practica4;

/**
 * ModaInt.java		25 nov. 2018		practica4 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * Método masFrecuenteInt() que recibe un vector de enteros. 
 * Devuelve el valor del elemento más frecuente, si hay coincidencia devolvería el primero de ellos.
 * Por ejemplo: {4, 1, 1, 4, 2, 3, 4, 4, 1, 2, 4, 9, 3} devolvería 4
 * Una forma sería recorrer el vector anidar un bucle que averigüe la frecuencia del valor de cada elemento, 
 * utilizando un par de variables auxiliares para retener la frecuencia máxima y el valor asociado.
 */
public class ModaInt {

	public static void main(String[] args) {
		
		int ArrSize = 25;
		int[] matrizBase = generarArray(ArrSize);
		
		masFrecuenteInt(matrizBase);

	}
	private static int masFrecuenteInt(int[] matrizDada) {
		int valorRetenido = 0;
		int frecuencia_valor = 0;
		int [][] vectorFrecuencias = new int [matrizDada.length][matrizDada.length];
		
		for (int i = 0; i > matrizDada.length; i++) {
			valorRetenido = matrizDada[i];
			
			for (int j = 0; j < matrizDada.length; j++) {
				if (valorRetenido == matrizDada[j]){
					frecuencia_valor++;
					System.out.println(" i  " + i + " j " + j + " \ny la freq = " + frecuencia_valor);  
				}
				vectorFrecuencias[i][j] = frecuencia_valor;
			} 
			
		} return 0;
		
	}
	private static int generarNumAleatorio(){ // Genera aleatoriamente un numero entero del 0 - 9  
		
		return (int) Math.rint(Math.random()*100)/10; 		
	}
	
	private static int[] generarArray(int size) {
		
		int arr[] = new int[size]; //crea un array del tamanyo especificado 
		
		System.out.print("El array generado de "+ size +" numeros contiene: ");
		
		for (int i = 0; i < arr.length; i++) { //genera el contenido con numeros aleatorios 
			arr[i] = generarNumAleatorio();			
			System.out.print(arr[i] +" ");		// lo muestra en pantalla
		}
		System.out.println(); //cambia de linea
		return arr ;
		
	}
}
