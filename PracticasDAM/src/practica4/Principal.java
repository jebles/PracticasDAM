package practica4;

/**
 * Principal.java		18 dic. 2018		practica de autoevaluacion 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1 
 * El método -cargarDatosUsuariosPatron(patron)- recibe como parámetro de entrada, un vector de enteros 
 * -patron()- con unos y ceros que indican los elementos que deberán quedar vacíos, o no, en el vector datosUsuarios 
 * cuando se vayan añadiendo datos con valores diferentes. 
 */


class Usuario {

	//Atributos
	public String nif;
	public String nombre;
	public String apellidos;
	public String correo;
	public String domicilio;
	public String fechaNacimiento;
	public String fechaAlta;
	public String claveAcceso;
	public String rol;


	public Usuario(Usuario usr) {

		setNif(nif);
		setNombre(nombre);
		setApellidos(apellidos);
		setCorreo(correo);
		setDomicilio(domicilio);
		setFechaNacimiento(fechaNacimiento);
		setFechaAlta(fechaAlta);
		setClaveAcceso(claveAcceso);
		setRol(rol);	
	}
	//constructor por defecto
	public Usuario() {

		this.nif = "12123123R";
		this.nombre = "Juanull";
		this.apellidos = "Arco";
		this.correo = "jarrco@gmail.com";
		this.domicilio = "Plata, 15";
		this.fechaNacimiento = "23/12/1981";
		this.fechaAlta = "30/12/2018";
		this.claveAcceso = "Mosra&12";
		this.rol = "ESTANDAR";
	}


	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getClaveAcceso() {
		return claveAcceso;
	}

	public void setClaveAcceso(String claveAcceso) {
		this.claveAcceso = claveAcceso;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}


} //class

public class Principal {

	final static int PATRON_SIZE = 7;
	final static int MAX_USUARIOS = 45;

	// Almacén de datos resuelto con arrays
	public static Usuario[] datosUsuarios = new Usuario[MAX_USUARIOS];

	public static void main(String[] args) {

		//declaracion array patron
		int [] patron = new int [PATRON_SIZE];

		rellenarPatronRdm(patron);		
		cargarDatosUsuariosPatron(patron);
	}

	/** 
	 * Método que recibe el patron generado con 0 y 1 de tamaño definido en la variable global y lo llena 
	 * con obejtos de la clase Usuario cada 1 que encuentra dentro del patron, y repite la duracion del mismo
	 * hasta completar el tamaño de MAX_USUARIOS
	 * @param patron generado
	 * Imprime por consola el contenido final del array datosUsuarios 
	 */	
	private static void cargarDatosUsuariosPatron(int [] patrn) {

		for (int i = 0; i < MAX_USUARIOS; i++ ) {		

			if (patrn[i % PATRON_SIZE] == 1) {
				datosUsuarios[i] = new Usuario();
			} 				
			System.out.println(datosUsuarios[i]);			
		}
	}

	/**
	 *  Este metodo rellena el patron con 0 y 1 de manera aleatoria cada vez que se ejecuta y lo imprime
	 *  @param patron
	 *  imprime el patron por pantalla    
	 **/
	private static int[] rellenarPatronRdm(int[] patr) {
		
		System.out.print("El patron es: ");
		
		for (int i = 0; i < patr.length; i++) {				 
			patr [i] = (int) Math.rint(Math.random());
			System.out.print(patr[i]);
		}		
		System.out.println();
		return patr;
	}

} //class


