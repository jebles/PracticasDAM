package practica4;

/**
 * Letras.java		28 nov. 2018		practica4 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 *  	

    Escribe un método que se llame bannerLetra() que recibe un carácter como parámetro y devuelve una cadena de caracteres con el texto, 
    formateado sobre una matriz de 7x7, de la letra correspondiente al carácter recibido.
    Por ejemplo si se introduce la H, la cadena devuelta, al imprimirla, daría:

       H     H

       H     H

       H     H

       HHHHHHH

       H     H     

       H     H

       H     H

        Sólo se admiten Letras en mayúsculas.
        Prueba exhaustivamente el métodos pedido desde main().
 * 
 */
import java.util.Scanner;

public class Letras {

	static public char vacio = 'X';

	public static void main(String[] args) {	

		char letraUsuario = pedirLetraUsuario().charAt(0);

		bannerLetra(letraUsuario);
	}
	private static String pedirLetraUsuario() {
		System.out.println("Introduce una letra: ");
		return new Scanner(System.in).next();

	}	
	private static void bannerLetra(char letra) {

		char matrizLetra [] [] = new  char [7] [7]; //declarada la matriz 
		//deja por defecto la matriz llena de espacios		

		switch (letra) {
		case 'A': 
			matrizLetra  = escribe_linea(letra, matrizLetra, 0, 2, 4); 
			matrizLetra = escribe_columna(letra, matrizLetra, 1, 1, 6);
			matrizLetra = escribe_columna(letra, matrizLetra, 5, 1, 6);
			matrizLetra = escribe_linea(letra, matrizLetra, 3, 2, 4);		
			imprimirMatriz(matrizLetra);
			break;
		case 'B':
			matrizLetra  = escribe_linea(letra, matrizLetra, 0, 1, 4); 
			matrizLetra = escribe_columna(letra, matrizLetra, 1, 0, 6);
			matrizLetra = escribe_columna(letra, matrizLetra, 5, 1, 2);
			matrizLetra = escribe_columna(letra, matrizLetra, 5, 4, 5);
			matrizLetra  = escribe_linea(letra, matrizLetra, 3, 1, 4); 
			matrizLetra = escribe_columna(letra, matrizLetra, 5, 4, 5);
			matrizLetra = escribe_linea(letra, matrizLetra, 6, 1, 4);		
			imprimirMatriz(matrizLetra);
			break;
		case 'C':
			matrizLetra  = escribe_linea(letra, matrizLetra, 0, 2, 4);
			matrizLetra = escribe_columna(letra, matrizLetra, 1, 1, 5);
			matrizLetra = escribe_columna(letra, matrizLetra, 5, 1, 1);
			matrizLetra = escribe_columna(letra, matrizLetra, 5, 5, 5);
			matrizLetra  = escribe_linea(letra, matrizLetra, 6, 2, 4); 			
			imprimirMatriz(matrizLetra);
			break;
		case 'D':
			matrizLetra  = escribe_linea(letra, matrizLetra, 0, 1, 4); 
			matrizLetra = escribe_columna(letra, matrizLetra, 1, 0, 6);
			matrizLetra  = escribe_linea(letra, matrizLetra, 6, 1, 4);
			matrizLetra = escribe_columna(letra, matrizLetra, 5, 1, 5);
			imprimirMatriz(matrizLetra);
			break;
		case 'E':
			matrizLetra  = escribe_linea(letra, matrizLetra, 0, 1, 5);
			matrizLetra  = escribe_linea(letra, matrizLetra, 3, 1, 5);
			matrizLetra  = escribe_linea(letra, matrizLetra, 6, 1, 5);
			matrizLetra = escribe_columna(letra, matrizLetra, 1, 0, 6);
			imprimirMatriz(matrizLetra);
			break;
		case 'F':
			matrizLetra  = escribe_linea(letra, matrizLetra, 0, 1, 5);
			matrizLetra = escribe_columna(letra, matrizLetra, 1, 0, 6);
			matrizLetra = escribe_columna(letra, matrizLetra, 3, 1, 4);
			imprimirMatriz(matrizLetra);
			break;
		case 'G':
			matrizLetra  = escribe_linea(letra, matrizLetra, 0, 2, 4);
			matrizLetra = escribe_columna(letra, matrizLetra, 1, 1, 5);
			matrizLetra = escribe_columna(letra, matrizLetra, 1, 1, 5);
			matrizLetra = escribe_columna(letra, matrizLetra, 5, 1, 1);	
			matrizLetra = escribe_columna(letra, matrizLetra, 5, 4, 5);	
			matrizLetra = escribe_columna(letra, matrizLetra, 4, 4, 4);
			matrizLetra  = escribe_linea(letra, matrizLetra, 6, 2, 4); 	
			imprimirMatriz(matrizLetra);
			break;
		case 'H':
			matrizLetra = escribe_columna(letra, matrizLetra, 1, 0, 6);
			matrizLetra = escribe_columna(letra, matrizLetra, 5, 0, 6);
			matrizLetra = escribe_linea(letra, matrizLetra, 3, 2, 4);
			imprimirMatriz(matrizLetra);
			break;
		case 'I':
			matrizLetra  = escribe_linea(letra, matrizLetra, 0, 2, 4);
			matrizLetra  = escribe_linea(letra, matrizLetra, 6, 2, 4);
			matrizLetra = escribe_columna(letra, matrizLetra, 3, 0, 6);
			imprimirMatriz(matrizLetra);
			break;
		case 'J':
			matrizLetra  = escribe_linea(letra, matrizLetra, 0, 1, 5);
			matrizLetra = escribe_columna(letra, matrizLetra, 5, 1, 5);
			matrizLetra = escribe_columna(letra, matrizLetra, 1, 5, 5);
			matrizLetra  = escribe_linea(letra, matrizLetra, 6, 2, 4); 			
			imprimirMatriz(matrizLetra);
			break;
		case 'K':
			matrizLetra = escribe_columna(letra, matrizLetra, 1, 0, 6);
			matrizLetra  = escribe_linea(letra, matrizLetra, 0, 5, 5);
			matrizLetra  = escribe_linea(letra, matrizLetra, 1, 4, 4);
			matrizLetra  = escribe_linea(letra, matrizLetra, 2, 3, 3);
			matrizLetra  = escribe_linea(letra, matrizLetra, 3, 1, 2); 	
			matrizLetra  = escribe_linea(letra, matrizLetra, 6, 5, 5);
			matrizLetra  = escribe_linea(letra, matrizLetra, 5, 4, 4);
			matrizLetra  = escribe_linea(letra, matrizLetra, 4, 3, 3);		
			imprimirMatriz(matrizLetra);
			break;
		case 'L':
			matrizLetra = escribe_columna(letra, matrizLetra, 1, 0, 6);
			matrizLetra  = escribe_linea(letra, matrizLetra, 6, 1, 6);
			imprimirMatriz(matrizLetra);
			break;
		case'M':
			matrizLetra = escribe_columna(letra, matrizLetra, 0, 0, 6);
			matrizLetra  = escribe_linea(letra, matrizLetra, 1, 1, 1);
			matrizLetra  = escribe_linea(letra, matrizLetra, 2, 2, 2);
			matrizLetra  = escribe_linea(letra, matrizLetra, 3, 3, 3); //mitad
			matrizLetra  = escribe_linea(letra, matrizLetra, 1, 5, 5);
			matrizLetra  = escribe_linea(letra, matrizLetra, 2, 4, 4);
			matrizLetra = escribe_columna(letra, matrizLetra, 6, 0, 6);

			imprimirMatriz(matrizLetra);
			break;
		case 'N':
			matrizLetra = escribe_columna(letra, matrizLetra, 0, 0, 6);
			matrizLetra  = escribe_linea(letra, matrizLetra, 1, 1, 1);
			matrizLetra  = escribe_linea(letra, matrizLetra, 2, 2, 2);
			matrizLetra  = escribe_linea(letra, matrizLetra, 3, 3, 3); //mitad
			matrizLetra  = escribe_linea(letra, matrizLetra, 4, 4, 4);
			matrizLetra  = escribe_linea(letra, matrizLetra, 5, 5, 5);
			matrizLetra = escribe_columna(letra, matrizLetra, 6, 0, 6);
			imprimirMatriz(matrizLetra);
			break;
		case 'O':
			matrizLetra = escribe_columna(letra, matrizLetra, 1, 1, 5);
			matrizLetra = escribe_columna(letra, matrizLetra, 5, 1, 5);
			matrizLetra  = escribe_linea(letra, matrizLetra, 0, 2, 4);
			matrizLetra  = escribe_linea(letra, matrizLetra, 6, 2, 4);
			imprimirMatriz(matrizLetra);
			break;
		case 'P':
			matrizLetra  = escribe_linea(letra, matrizLetra, 0, 1, 4);
			matrizLetra = escribe_columna(letra, matrizLetra, 1, 1, 6);
			matrizLetra = escribe_columna(letra, matrizLetra, 5, 1, 2);
			matrizLetra = escribe_linea(letra, matrizLetra, 3, 1, 4);
			imprimirMatriz(matrizLetra);
			break;
		case 'Q':
			matrizLetra = escribe_columna(letra, matrizLetra, 1, 1, 5);
			matrizLetra = escribe_columna(letra, matrizLetra, 5, 1, 5);
			matrizLetra  = escribe_linea(letra, matrizLetra, 0, 2, 4);
			matrizLetra  = escribe_linea(letra, matrizLetra, 6, 2, 4);
			matrizLetra  = escribe_linea(letra, matrizLetra, 4, 4, 4);
			matrizLetra  = escribe_linea(letra, matrizLetra, 6, 6, 6);
			imprimirMatriz(matrizLetra);
			break;
		case 'R':
			matrizLetra  = escribe_linea(letra, matrizLetra, 0, 1, 4);
			matrizLetra = escribe_columna(letra, matrizLetra, 1, 1, 6);
			matrizLetra = escribe_columna(letra, matrizLetra, 5, 1, 2);
			matrizLetra = escribe_linea(letra, matrizLetra, 3, 1, 4);
			matrizLetra = escribe_linea(letra, matrizLetra, 4, 3, 3);
			matrizLetra = escribe_linea(letra, matrizLetra, 5, 4, 4);
			matrizLetra = escribe_linea(letra, matrizLetra, 6, 5, 5);
			imprimirMatriz(matrizLetra);
			break;

		case 'S':
			matrizLetra = escribe_linea(letra, matrizLetra, 0, 2, 4);
			matrizLetra = escribe_linea(letra, matrizLetra, 6, 2, 4);
			matrizLetra = escribe_linea(letra, matrizLetra, 3, 2, 4);
			matrizLetra = escribe_linea(letra, matrizLetra, 1, 1, 1);
			matrizLetra = escribe_linea(letra, matrizLetra, 1, 5, 5);
			matrizLetra = escribe_linea(letra, matrizLetra, 2, 1, 1);
			matrizLetra = escribe_linea(letra, matrizLetra, 5, 1, 1);
			matrizLetra = escribe_linea(letra, matrizLetra, 4, 5, 5);
			matrizLetra = escribe_linea(letra, matrizLetra, 5, 5, 5);
			imprimirMatriz(matrizLetra);
			break;
		case 'T':
			matrizLetra  = escribe_linea(letra, matrizLetra, 0, 0, 6);
			matrizLetra = escribe_columna(letra, matrizLetra, 3, 1, 6);
			imprimirMatriz(matrizLetra);
			break;
		case 'U':
			matrizLetra = escribe_columna(letra, matrizLetra, 1, 1, 5);
			matrizLetra = escribe_columna(letra, matrizLetra, 6, 1, 5);
			matrizLetra = escribe_linea(letra, matrizLetra, 6, 2, 5);
			imprimirMatriz(matrizLetra);
			break;
		case 'V':
			matrizLetra = escribe_columna(letra, matrizLetra, 1, 1, 4);
			matrizLetra = escribe_columna(letra, matrizLetra, 5, 1, 4);
			matrizLetra = escribe_linea(letra, matrizLetra, 6, 3, 3);
			matrizLetra = escribe_linea(letra, matrizLetra, 5, 2, 2);
			matrizLetra = escribe_linea(letra, matrizLetra, 5, 4, 4);
			imprimirMatriz(matrizLetra);
			break;
		case 'X':
			matrizLetra = escribe_diagonal_abajo(letra, matrizLetra,  0, 6); 
			matrizLetra = escribe_diagonal_arriba(letra, matrizLetra, 6, 0); 
			imprimirMatriz(matrizLetra);
			break;
		case 'Y':
			matrizLetra = escribe_diagonal_abajo(letra, matrizLetra,  0, 3); 
			matrizLetra = escribe_diagonal_arriba(letra, matrizLetra, 3, 0); 
			imprimirMatriz(matrizLetra);
			break;


		}
	}
	private static void imprimirMatriz(char[][] matrizLetra) {
		for (int i = 0; i <matrizLetra.length; i++) {			
			for (int j = 0; j < matrizLetra.length;j++) {
				System.out.print(matrizLetra [i] [j]);
			}System.out.println(); 					
		}
	}
	private static char[][] escribe_linea(char letra, char[][] matriz, int valor_j, int inicio_linea, int fin_linea) {	

		for (int i = inicio_linea; i <= fin_linea; i++ ) {			
			matriz [valor_j] [i] = letra;			
		}
		return matriz; 
	}

	private static char[][] escribe_columna(char letra, char[][] matriz, int valor_i, int inicio_col, int fin_columna) {		

		for (int j = inicio_col; j <= fin_columna; j++ ) {			
			matriz [j] [valor_i] = letra;			
		}
		return matriz; 
	}	
	private static char[][] escribe_diagonal_abajo(char letra, char[][] matriz,  int inicio_diag, int fin_diag) {		
		int contador_aux = 0;
		for (int r = inicio_diag; r <= fin_diag; r++ ) {			
			matriz [r] [contador_aux] = letra;
			contador_aux++;		
		}
		return matriz; 
	}	



	private static char[][] escribe_diagonal_arriba(char letra, char[][] matriz, int inicio_diag, int fin_diag) {		
		int contador_aux = 0;
		for (int r = inicio_diag; r >= fin_diag; r-- ) {			
			matriz [contador_aux] [r] = letra;
			contador_aux++;
		}
		return matriz; 
	}
}




