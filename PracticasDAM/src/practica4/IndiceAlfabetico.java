package practica4;

/**
 * IndiceAlfabetico.java		24 dic. 2018		practica4 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * Escribe un método que se llame indiceAfabetico() que recibe una palabra como texto. 
 * Devuelve un array (vector) de números conteniendo la posición que le corresponde a cada letra de la palabra
 * recibida según el alfabeto español.
 * a=97
 */
public class IndiceAlfabetico {

	public static void main(String[] args) {
		
		String palabra = "mañanazo";		
		indiceAfabetico(palabra);		
	}

	private static int [] indiceAfabetico(String word) {
		
		int[] num = new int [word.length()];
		int[] indiceES = new int [word.length()];
		
		for (int i = 0; i < word.length(); i++) {
			Character letra = word.charAt(i);
			
			//mete el numero UNICODE a un array de int
			num[i] = letra.hashCode();			
			deUNICODEaINDICEes(num, indiceES, i);
			// indiceES[i] =deUNICODEaINDICEes(letra.hasCode)
		}		
		return indiceES;
	}

	public static void deUNICODEaINDICEes(int[] num, int[] indiceES, int i) {
		//eq vuilaencia de num codigo UNICODE a orden alfabeto
		
		if (num[i] <= 110) {
			indiceES[i] = num[i]-96;
		}
		if (num[i] > 110) {
			indiceES[i] = num[i]-95;
		}
		if (num[i] == 241) { // la ñ
			indiceES[i] = num[i]-226;
		}
		
		System.out.print(indiceES[i] + " ");
	}

}
