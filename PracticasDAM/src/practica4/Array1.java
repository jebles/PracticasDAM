package practica4;

/**
 * Array1.java 22 nov. 2018  practica4 
 * @author Jesús Pérez Robles  NRE4004010 
 * Escribe un programa simple que utilice un array (vector) de 10 elementos de números enteros e inicialice cada elemento 
 * con el valor 7 en cada elemento. Los elementos del vector se pueden mostrar en la pantalla utilizando un bucle for.
 * Se recomienda utilizar todos los modos de inicialización conocidos para un vector int[]. 
 */
public class Array1 {

	public static void main(String[] args) {

		//declaracion tipo 1
		int[] vector1 = new int[10];		
		
		//declaracion tipo 2
		int[] vector2;
		vector2 = new int [10];
		
		
		System.out.print("El vector1 contiene: ");
		llenar_imprimir_array(vector1);
		
		System.out.print("El vector2 contiene: ");
		llenar_imprimir_array(vector2);

	}

	private static void llenar_imprimir_array(int[] vector) {
		for (int i = 0; i < vector.length; i++) {
			vector[i]=7;						//anyade el valor a cada posición dentro del array
			System.out.print(vector[i]);	// saca por pantalla cada valor			

		} System.out.println(); //nueva linea
	}

}
