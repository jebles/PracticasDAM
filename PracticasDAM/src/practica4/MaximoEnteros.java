package practica4;


/**
 * MaximoEnteros.java		25 nov. 2018		 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * método que se llama maximoIntConsecutivos() que recibe un vector de enteros. 
 * Devuelve cuantos valores son consecutivos entre sus elementos. Si devuelve 1 todos los elementos son diferentes.
 * Por ejemplo: {3, 5, 3, 4, 5, 5, 4} devolvería 2. 
 * Recorriendo los datos de izquierda a derecha, empezando por el segundo elemento hay que comprobar si es secuencia del anterior. 
 * Se puede utilizar un contador iniciado en 1 que se incrementa al comprobarse que un elemento es consecutivo del anterior. 
 * Cada vez que se inicia nueva secuencia hay que guardar el valor del contador en una variable auxiliar con el máximo alcanzado.
 */
public class MaximoEnteros {
	
	static final int arraySize = 40;

	public static void main(String[] args) {
		
		int[] matrizEnteros = generarArray(arraySize);
		
		System.out.println("El numero de coincidencias de enteros consecutivos en el array generado es de " +maximoIntConsecutivos(matrizEnteros));

	}

	private static int maximoIntConsecutivos(int[] vectorInts) { 
		
		int contador = 0;		
		
		for (int i = 1 ; i < vectorInts.length ; i++) {
			
			if ( vectorInts[i] == vectorInts[i-1]+1) {  // aumenta el contador cada vez que se produce una sucesion consecutiva entre 
														// dos numeros recorriendo el array de izq a dcha
				contador ++;
				System.out.println("En la posicion "+ i +" se ha encontrado que el int "+ vectorInts[i-1] 
						+ " precede a un " 
						+ vectorInts[i] +" por lo que el contador lleva: " + contador);
			}
		}
		return contador;
	}
	private static int generarNumAleatorio() { // Genera aleatoriamente un numero entero del 0 - 9  
		return (int) Math.rint(Math.random()*100)/10; 		
	}
	
	private static int[] generarArray(int tamanyo) {
		
		int arr[] = new int[tamanyo]; //crea un array del tamanyo especificado 
		
		System.out.print("El array generado de "+ tamanyo +" numeros contiene: ");
		
		for (int i = 0; i < arr.length; i++) { //genera el contenido con numeros aleatorios 
			arr[i] = generarNumAleatorio();			
			System.out.print(arr[i] +" ");		// lo muestra en pantalla
		}
		System.out.println(); //cambia de linea
		return arr ;
		
	}

}
