package practica4;

/**
 * BuscarEntero.java		25 nov. 2018		practica4 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * Método buscarInt() que recibe un vector de enteros y un valor a buscar dentro del vector. 
 * Devuelve el índice de la primera ocurrencia dentro del vector proporcionado.
 * Por ejemplo: {3, 2, 3, 4, 7, 2, 4} si se busca 4 devolvería 3.
 * Hay que recorrer el vector secuencialmente y si se encuentra hay que terminar sin llegar al final del recorrido.
 */
import java.util.Scanner;

public class BuscarEntero {

	private static int arraySize = 15;
	

	public static void main(String[] args) {
		
		
		int[] vectorEnteros = generarArray(arraySize);
		int valor_a_buscar = pedirNumero();
		
		System.out.println("El número elegido se ha localizado en el índice " + buscarInt(vectorEnteros, valor_a_buscar));
		

	}

	private static int buscarInt(int[] matrizEnteros, int numero_objetivo) {
		int indice_coincidencia = 0;
		
		for (int i = 0; i < matrizEnteros.length; i++) {
			
			if (matrizEnteros[i] == numero_objetivo) {
				indice_coincidencia = i;
				break;
			}
		}
		return indice_coincidencia;
		
	}
	
	private static int generarNumAleatorio(){ // Genera aleatoriamente un numero entero del 0 - 9  
		
		return (int) Math.rint(Math.random()*100)/10; 		
	}
	
	private static int[] generarArray(int tamanyo) {
		
		int arr[] = new int[tamanyo]; //crea un array del tamanyo especificado 
		
		System.out.print("El array generado de "+ tamanyo +" numeros contiene: ");
		
		for (int i = 0; i < arr.length; i++) { //genera el contenido con numeros aleatorios 
			arr[i] = generarNumAleatorio();			
			System.out.print(arr[i] +" ");		// lo muestra en pantalla
		}
		System.out.println(); //cambia de linea
		return arr ;
		
	}
	
	private static int pedirNumero() {
		System.out.println("Por favor, introduzca un numero del 0 al 9 para ver el indice de la primera coincidencia del mismo dentro del vector generado: ");
		return  new Scanner(System.in).nextInt();

	} 
	
}
