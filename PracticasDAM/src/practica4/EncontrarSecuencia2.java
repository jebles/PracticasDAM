package practica4;


/**
 * Secuencia.java		27 nov. 2018		practica4 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * método buscarSecuenciaInt() que recibe dos vectores de enteros, 
 * el segundo es una secuencia de valores a buscar en el primer vector. 
 * Devuelve el índice donde empieza la primera ocurrencia de la secuencia buscada.
 * Por ejemplo: {4, 3, 1, 4, 2, 5, 8} y {4, 2, 5} devolvería 3.
 * Hay que recorrer el vector secuencialmente comprobando coincidencia con el primer elemento de la secuencia buscada. 
 * Después hay que comprobarla completamente con otro bucle. 
 * Si hay coincidencia completa termina sin llegar al final del recorrido.
 */
public class EncontrarSecuencia2 {


	public static void main(String[] args) {	
					//   0  1  2  3  4  5  6  7  8  9  10  11  12
		int VectorA[] = {4, 3, 1, 4, 2, 5, 8, 4, 4, 2, 5,  6,  14, 2, 3};
		int VectorB[] = {4, 4, 2};

		System.out.println("El indice buscado es " + buscarSecuenciaInt(VectorA,VectorB));		
	}

	private static int buscarSecuenciaInt(int[] A, int[] B) {

		int indice = -1;
		boolean encontrado = false;
		int contador = 0;
		System.out.println("El tamaño de B es: " + Math.subtractExact(B.length, 1));
		
		
		for (int i = 0; i < A.length-1; i++) { //recorre el vector A con el contador i

			//para el caso de un array B de un solo elemento.			
			if (A[i] == B[0] && B.length-1 < 1) {
				indice = i;
				encontrado = true;
			}

			for (int j = 0; j < B.length-1; j++) { // recorre el vector B con el contador j


				if (A[i] == B[0] && encontrado == false) {	
					contador = 0;
					int x = i;
					int y = j;

					System.out.println("localizado B[cero] = A[" + i +"]");
					indice = i; //guarda el indice donde comienza la coincidenca de a			
					
					while (A[x] == B[y] && encontrado == false) { 
						contador++;			
						System.out.println("localizado A["+x+"] = B[" + y + "]\n contamos un acierto: "
								+ " el contador va por " + contador);						
						System.out.println("El tamaño de B es: " + Math.subtractExact(B.length, 1));

						if (contador == B.length) {							
							encontrado = true;
							break;
						}
						x++;
						y++;

					}

		
				} 
			} 
		}
		if (encontrado == false) {
			indice = -1;
			System.out.println("la secuencia del vector B no ha sido encontrada en el vector A");
		} 
		else {
			System.out.println("Vector encontrado. Saliendo...");
		}
		return indice;
	}


}	
