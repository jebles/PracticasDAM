package practica4;


/**
 * Frecuencia.java		24 nov. 2018		practica4 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * Escribe un método que se llame frecuenciaNumero() 
 * que recibe un vector de enteros y un número. 
 * Devuelve la frecuencia entre los elementos del vector del número recibido.
 */
public class Frecuencia {

	static public int ARRAY_SIZE = 45;
	
	public static void main(String[] args) {
		
		int[] arrFrecuencia = new int[ARRAY_SIZE] ;
		int numero = 0;
		int ocurrencias_num = 0 ;
		
		numero = generarNumAleatorio();
		
		System.out.println("El numero generado aleatoriamente entre el 0 y el 12 es el: "+ numero + "\n");
		
		arrFrecuencia = generarArray(ARRAY_SIZE);	
		
		ocurrencias_num = frecuenciaNumero(arrFrecuencia, numero);
		
		System.out.println("\nEl numero  "+ numero +" Se ha encontrado " + ocurrencias_num + " veces dentro del array. \n"
				+ "Fin del programa...");		
		
	}
	
	private static int frecuenciaNumero(int[] arr, int num) {	 //recoge el array y el numero generado 	
		int num_ocurrencias = 0;		
		
		for (int ocurrencia : arr) { //por cada elemento del array que recorre el bucle for each
			
			if (ocurrencia == num) {// en caso de coicidir con el numero generado
				num_ocurrencias++;	//hace +1 en el numero de ocurrencias
			}
		}	
		return num_ocurrencias;	//Devuelve las veces que se ha hallado.

	}
	private static int generarNumAleatorio() { // Genera aleatoriamente un numero entero del 0 - 9  
		return (int) Math.rint(Math.random()*100)/10; 		
	}
	
	private static int[] generarArray(int tamanyo) {
		
		int arr[] = new int[tamanyo]; //crea un array del tamanyo especificado 
		
		System.out.print("El array generado de "+ tamanyo +" numeros contiene: ");
		
		for (int i = 0; i < arr.length; i++) { //genera el contenido con numeros aleatorios 
			arr[i] = generarNumAleatorio();			
			System.out.print(arr[i] +" ");		// lo muestra en pantalla
		}
		return arr ;
		
	}

}
