package practica4;

/**
 * VectorOrdenado.java		25 nov. 2018		practica4 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * Escribe un método que se llame yaOrdenadoInt() que recibe un vector de enteros. Devuelve true si está ordenado.
 * Por ejemplo: {3, 2, 3, 4, 7, 2, 4} devolvería false.
 * Hay que recorrer el vector secuencialmente comprobando por parejas y si se encuentra un par de elementos desordenados 
 * hay que terminar sin llegar al final del recorrido.
 */
public class VectorOrdenado {

	public static void main(String[] args) {
		//instanciado de arrays
		int[] matrizA = {1,2,3,4,5,6,7,8,9,10};
		int[] matrizB = {11,12,13,16,15,14,18,19,20,21};

		//arroja resultados
		System.out.println("La condición de ordenación del vector es una progresión aritmética de diferencia constante 1.");
		System.out.println("ArrayA sorting condition is " + yaOrdenadoInt(matrizA));
		System.out.println("ArrayB sorting condition is " + yaOrdenadoInt(matrizB));		

	}

	private static boolean yaOrdenadoInt(int[] vector) {
		boolean resultado = false;

		for (int i = 1; i < vector.length; i++) {  //comprueba si se cumple la condicion de progresión == +1 
			if (vector[i-1]+1 == vector[i]) {
				//System.out.println("verificando " + vector[i-1] + " con " + vector[i]);
				resultado = true;
			} else {
				resultado = false;
				break; // cuando se de este caso, sale del bucle
			} 
		} return resultado;		
	}
}
