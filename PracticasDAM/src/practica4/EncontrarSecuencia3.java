package practica4;


/**
 * Secuencia.java		27 nov. 2018		practica4 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * método buscarSecuenciaInt() que recibe dos vectores de enteros, 
 * el segundo es una secuencia de valores a buscar en el primer vector. 
 * Devuelve el índice donde empieza la primera ocurrencia de la secuencia buscada.
 * Por ejemplo: {4, 3, 1, 4, 2, 5, 8} y {4, 2, 5} devolvería 3.
 * Hay que recorrer el vector secuencialmente comprobando coincidencia con el primer elemento de la secuencia buscada. 
 * Después hay que comprobarla completamente con otro bucle. 
 * Si hay coincidencia completa termina sin llegar al final del recorrido.
 */
public class EncontrarSecuencia3 {


	public static void main(String[] args) {	
					//   0  1  2  3  4  5  6  7  8  9  10  11  12  13 14
		int VectorA[] = {4, 3, 1, 4, 2, 5, 8, 4, 4, 2, 5,  6,  14, 2, 3};
		int VectorB[] = {4, 3, 1};

		System.out.println("El indice buscado es " + buscarSecuenciaInt(VectorA,VectorB));		
	}

	private static int buscarSecuenciaInt(int[] A, int[] B) {

		int indice = -1;
		int x = 0;
		int j =0;

		System.out.println("El tamaño de B es: " + Math.subtractExact(B.length, 1));


		for (int i = 0; i < A.length-1; i++) { //recorre el vector A con el contador i	

			if (A[i] == B[0]) {
				
			x = i;
			indice = i;
			
			while (A[x] == B[j]) {
				
				System.out.println("entra en el bucle, compara A[x]"+A[x]+ " con B[j]" + B[j] );		
				x++;
				j++;
				System.out.println(" +1 a x -> "+x + " +1 a j -> "+j);
				if ( j == B.length) {
					break;
				}
			}
			}
		}
		return indice;
	}
}	
