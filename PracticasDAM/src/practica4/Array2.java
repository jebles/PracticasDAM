package practica4;

/**
 * Array2.java		22 nov. 2018		practica4 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * Escribe un programa simple que utilice un array (vector) de 20 elementos de números enteros 
 * e inicialice cada elemento con un valor igual al índice correspondiente del elemento, multiplicado por 5.
 * Los elementos del vector se pueden mostrar en la pantalla utilizando un bucle for.
 * Se recomienda utilizar todos los modos de inicialización conocidos para un vector int[].
 */
public class Array2 {

	public static void main(String[] args) {
		
		//declaracion tipo 1
		int[] vector1 = new int[20];
		
		//declaracion tipo 2
		int [] vector2;
		vector2 = new int[20];
		
		//sacamos por pantalla los requisitos

		System.out.println("El vector1 contiene: ");	
		calcularEimprimirArray(vector1);

		System.out.println("El vector2 contiene: ");
		calcularEimprimirArray(vector2);


	}

	private static void calcularEimprimirArray(int[] vector) { //bucle que va anyadiendo los valores al array a razon de *5 cada iteración
		for (int i = 0; i < vector.length; i++) {
			vector[i] = i*5;
			System.out.print("indice "+ i +": " + vector[i]+" \n"); //y lo imprime
			
		}
	}

}
