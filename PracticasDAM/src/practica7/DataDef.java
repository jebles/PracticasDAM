package practica7;

import java.util.GregorianCalendar;

/**
 * DataDef.java		12 mar. 2019		practica7 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * ejercicio 2,3
    Identifica los datos que consideras adecuados para representar el estado de los siguientes objetos 
    teniendo en cuenta el contexto en el que se vayan a utilizar:
               
        
        
        11.Un disco de música (en una discoteca).
        12.Un teléfono móvil (en una tienda de telefonía)
        13.Un teléfono móvil (en el sistema de una empresa de telecomunicaciones)
        14.Un ordenador (en una tienda de Informática)
        15.Un ordenador (en una red de ordenadores)
        16.Un ordenador (en el inventario de una organización)

    Crea las correspondientes clases en Java.
    Define los constructores típicos de cada una de las clases. 
    Implementa los correspondientes métodos para el acceso y la modificación del estado de los objetos 
    (los métodos get y set).
 * 
 */
public class DataDef {

	//1. Un punto en un espacio cartesiano
	private class Punto {
		
		private int a;
		private int	b;
		
		//contructor por defecto con parámetros
		public Punto(int a, int b) {
			setA(a);
			setB(b);
		}
		//contructor por defecto vacío
		public Punto() {
			this(0,0);
		}
		public Punto(Punto p)  {
			setA(p.a);
			setB(p.b);
		}
		public int getA() {
			return a;
		}
		public void setA(int a) {
			this.a = a;
		}
		public int getB() {
			return b;
		}
		public void setB(int b) {
			this.b = b;
		}
	} //Punto class
	
    //2. Un segmento de recta (en un contexto geométrico).
	public class Segmento {
		
		private int rectaIni;
		private int rectaFin;
		private int segIni;
		private int segFin;
		
		public Segmento(int rectaIni, int rectaFin, int segIni, int segFin) {
			setRectaIni(rectaIni);
			setRectaFin(rectaFin);
			setSegIni(segIni);
			setSegFin(segFin);
		}
		public Segmento(int rectaIni, int rectaFin) {
			setRectaIni(rectaIni);
			setRectaFin(rectaFin);
			setSegIni(segIni);
			setSegFin(segFin);
		}
		public Segmento(Segmento segm) {
			rectaIni=segm.rectaIni;
			rectaFin=segm.rectaFin;
			segIni=segm.segIni;
			segFin=segm.segFin;
		}
		public Segmento() {
			this(0,0,0,0);
		}

		public int getRectaIni() {
			return rectaIni;
		}

		public void setRectaIni(int rectaIni) {
			this.rectaIni = rectaIni;
		}

		public int getRectaFin() {
			return rectaFin;
		}

		public void setRectaFin(int rectaFin) {
			this.rectaFin = rectaFin;
		}

		public int getSegIni() {
			return segIni;
		}

		public void setSegIni(int segIni) {
			assert segIni >= rectaIni;
			this.segIni = segIni;
		}

		public int getSegFin() {
			return segFin;
		}

		public void setSegFin(int segFin) {
			assert segFin <= rectaFin;
			this.segFin = segFin;
		}		
				
	}//Segmento class
	//3. Un polígono (en un contexto geométrico).
	public class Poligono {
		private boolean esRegular;
		private int lados;
		public Poligono(boolean es_regular, int lados) {
			setRegular(es_regular);
			setLados(lados);
		}
		public Poligono(Poligono poli) {
			this.esRegular=poli.esRegular;
			this.lados=poli.lados;
		}		
		public boolean isRegular() {
			return esRegular;
		}
		public void setRegular(boolean regular) {
			this.esRegular = regular;
		}
		public int getLados() {
			return lados;
		}
		public void setLados(int lados) {
			this.lados = lados;
		}
		
	}//Poligono class
	public class Manzana {
		private double peso;
		private String procedencia;
		private String variedad;
		private GregorianCalendar caducidad;
		public Manzana(double peso, String procedencia, String variedad, GregorianCalendar gregorianCalendar) {
			this.peso = peso;
			this.procedencia = procedencia;
			this.variedad = variedad;
			this.caducidad = gregorianCalendar;
		}
		public Manzana() {
			this(0.0,"nacional","reineta", new GregorianCalendar());
		}
		public Manzana(Manzana manz) {
			this.peso=manz.peso;
			this.procedencia=manz.procedencia;
			this.variedad=manz.variedad;
			this.caducidad=manz.caducidad;
		}
		public double getPeso() {
			return peso;
		}
		public void setPeso(double peso) {
			this.peso = peso;
		}
		public String getProcedencia() {
			return procedencia;
		}
		public void setProcedencia(String procedencia) {
			this.procedencia = procedencia;
		}
		public String getVariedad() {
			return variedad;
		}
		public void setVariedad(String variedad) {
			this.variedad = variedad;
		}
		public GregorianCalendar getCaducidad() {
			return caducidad;
		}
		public void setCaducidad(GregorianCalendar caducidad) {
			this.caducidad = caducidad;
		}
		
	}//Manzana class
	//5. Una carta (en Correos)
	public static class CartaPostal {
		private enum Tipo {cierre, autoadhesivo, sin_ventana, con_ventana};
		private enum Pago {normal, prepagado, acuse_recibo, por_avion, urgente};
		private enum Dimensiones {A4,A5,C65,Monarch,C5}; 
		private String color;
		private Tipo tipo_sobre;
		private Pago estado_pago;
		private Dimensiones tam;
		public CartaPostal(String color, Tipo tipo_sobre, Pago estado_pago, Dimensiones tam) {
			this.color = color;
			this.tipo_sobre = tipo_sobre;
			this.estado_pago = estado_pago;
			this.tam = tam;
		}
		public CartaPostal(CartaPostal carta) {
			this.color = carta.color;
			this.tipo_sobre = carta.tipo_sobre;
			this.estado_pago = carta.estado_pago;
			this.tam = carta.tam;
		}
		public CartaPostal() {
			this("Blanco",Tipo.cierre, Pago.normal, Dimensiones.C5);
		}
		public String getColor() {
			return color;
		}
		public void setColor(String color) {
			this.color = color;
		}
		public Tipo getTipo_sobre() {
			return tipo_sobre;
		}
		public void setTipo_sobre(Tipo tipo_sobre) {
			this.tipo_sobre = tipo_sobre;
		}
		public Pago getEstado_pago() {
			return estado_pago;
		}
		public void setEstado_pago(Pago estado_pago) {
			this.estado_pago = estado_pago;
		}
		public Dimensiones getTam() {
			return tam;
		}
		public void setTam(Dimensiones tam) {
			this.tam = tam;
		}
	}//class CartaPostal
	//6. Un libro (en una biblioteca)
	public static class Libro {
		public enum Estado {prestado, disponible, deteriorado, perdido};		
		private String ISBN;
		private String titulo;
		private String autor;
		private String editorial;
		private int edicion;
		private String categoria;
		private Estado estado;
				
		public Libro(String ISBN, String titulo, String autor, String editorial, int edicion, String categoria,
				Estado estado) {
			this.ISBN = ISBN;
			this.titulo = titulo;
			this.autor = autor;
			this.editorial = editorial;
			this.edicion = edicion;
			this.categoria = categoria;
			this.estado = estado;
		}
		public Libro() {
			this("ISBN","titulo","autor", 
					"editorial", 1, 
					"categoria", Estado.disponible);
		}
		public Libro (Libro lib) {
			this.ISBN = lib.ISBN;
			this.titulo = lib.titulo;
			this.autor = lib.autor;
			this.editorial = lib.editorial;
			this.edicion = lib.edicion;
			this.categoria = lib.categoria;
			this.estado = lib.estado;
		}
		public String getISBN() {
			return ISBN;
		}
		public void setISBN(String iSBN) {
			ISBN = iSBN;
		}
		public String getTitulo() {
			return titulo;
		}
		public void setTitulo(String titulo) {
			this.titulo = titulo;
		}
		public String getAutor() {
			return autor;
		}
		public void setAutor(String autor) {
			this.autor = autor;
		}
		public String getEditorial() {
			return editorial;
		}
		public void setEditorial(String editorial) {
			this.editorial = editorial;
		}
		public int getEdicion() {
			return edicion;
		}
		public void setEdicion(int edicion) {
			this.edicion = edicion;
		}
		public String getCategoria() {
			return categoria;
		}
		public void setCategoria(String categoria) {
			this.categoria = categoria;
		}
		public Estado getEstado() {
			return estado;
		}
		public void setEstado(Estado estado) {
			this.estado = estado;
		}	
	}//class Libro
	//7. Un libro (en una librería)
	public class Ejemplar {
		private String ISBN;
		private String titulo;
		private String autor;
		private String editorial;
		private int edicion;
		private String categoria;
		private double precio;
		private int existencias;
		public Ejemplar(String ISBN, String titulo, String autor, String editorial, int edicion, String categoria,
				double precio, int existencias) {
			this.ISBN = ISBN;
			this.titulo = titulo;
			this.autor = autor;
			this.editorial = editorial;
			this.edicion = edicion;
			this.categoria = categoria;
			this.precio = precio;
			this.existencias = existencias;
		}
		public Ejemplar() {
			this("ISBN","titulo","autor", 
					"editorial", 1, 
					"categoria", 0, 1);
		}
		public Ejemplar (Ejemplar lib) {
			this.ISBN = lib.ISBN;
			this.titulo = lib.titulo;
			this.autor = lib.autor;
			this.editorial = lib.editorial;
			this.edicion = lib.edicion;
			this.precio = lib.precio;
			this.existencias = lib.existencias;
		}
		public String getISBN() {
			return ISBN;
		}
		public void setISBN(String iSBN) {
			ISBN = iSBN;
		}
		public String getTitulo() {
			return titulo;
		}
		public void setTitulo(String titulo) {
			this.titulo = titulo;
		}
		public String getAutor() {
			return autor;
		}
		public void setAutor(String autor) {
			this.autor = autor;
		}
		public String getEditorial() {
			return editorial;
		}
		public void setEditorial(String editorial) {
			this.editorial = editorial;
		}
		public int getEdicion() {
			return edicion;
		}
		public void setEdicion(int edicion) {
			this.edicion = edicion;
		}
		public String getCategoria() {
			return categoria;
		}
		public void setCategoria(String categoria) {
			this.categoria = categoria;
		}
		public double getPrecio() {
			return precio;
		}
		public void setPrecio(double precio) {
			this.precio = precio;
		}
		public int getExistencias() {
			return existencias;
		}
		public void setExistencias(int existencias) {
			this.existencias = existencias;
		}
	}//class Ejemplar
	//8. Una canción (en una aplicación para un reproductor MP3).
	public class Cancion {
		private int bytes;
		private int duracionMseg;
		private double velocidad_muestreo;
		private String metadata;
		private String codificacion;
		private String URL;
		public Cancion(int bytes, int duracionMseg, double velocidad_muestreo, String metadata, String codificacion,
				String URL) {
			this.bytes = bytes;
			this.duracionMseg = duracionMseg;
			this.velocidad_muestreo = velocidad_muestreo;
			this.metadata = metadata;
			this.codificacion = codificacion;
			this.URL = URL;
		}
		public Cancion() {
			this(0,0,44.1,"meta","cod","url");
		}
		public Cancion(Cancion cancion) {
			this.bytes = cancion.bytes;
			this.duracionMseg = cancion.duracionMseg;
			this.velocidad_muestreo = cancion.velocidad_muestreo;
			this.metadata = cancion.metadata;
			this.codificacion = cancion.codificacion;
			this.URL = cancion.URL;
		}
		public int getBytes() {
			return bytes;
		}
		public void setBytes(int bytes) {
			this.bytes = bytes;
		}
		public int getDuracionMseg() {
			return duracionMseg;
		}
		public void setDuracionMseg(int duracionMseg) {
			this.duracionMseg = duracionMseg;
		}
		public double getVelocidad_muestreo() {
			return velocidad_muestreo;
		}
		public void setVelocidad_muestreo(double velocidad_muestreo) {
			this.velocidad_muestreo = velocidad_muestreo;
		}
		public String getMetadata() {
			return metadata;
		}
		public void setMetadata(String metadata) {
			this.metadata = metadata;
		}
		public String getCodificacion() {
			return codificacion;
		}
		public void setCodificacion(String codificacion) {
			this.codificacion = codificacion;
		}
		public String getURL() {
			return URL;
		}
		public void setURL(String uRL) {
			URL = uRL;
		}		
	}//class Cancion
	//9. Una canción (en una emisora de radio)
	public class CancionRadio {
		private int duracionMseg;
		private String titulo;
		private String artista;
		private String estilo;
		private boolean enPromocion;
		
		public CancionRadio(int duracionMseg, String titulo, String artista, String estilo,
				boolean enPromocion) {
			this.duracionMseg = duracionMseg;
			this.titulo = titulo;
			this.artista = artista;
			this.estilo = estilo;
			this.enPromocion = enPromocion;
		}
		public CancionRadio() {
			this(10000,"titulo", "artista", "estilo", false );
		}
		public CancionRadio(CancionRadio canci) {
			this.duracionMseg = canci.duracionMseg;
			this.titulo = canci.titulo;
			this.artista = canci.artista;
			this.estilo = canci.estilo;
			this.enPromocion = canci.enPromocion;
		}
		public int getDuracionMseg() {
			return duracionMseg;
		}
		public void setDuracionMseg(int duracionMseg) {
			this.duracionMseg = duracionMseg;
		}
		public String getTitulo() {
			return titulo;
		}
		public void setTitulo(String titulo) {
			this.titulo = titulo;
		}
		public String getArtista() {
			return artista;
		}
		public void setArtista(String artista) {
			this.artista = artista;
		}
		public String getEstilo() {
			return estilo;
		}
		public void setEstilo(String estilo) {
			this.estilo = estilo;
		}
		public boolean isEnPromocion() {
			return enPromocion;
		}
		public void setEnPromocion(boolean enPromocion) {
			this.enPromocion = enPromocion;
		}
	}//class CancionRadio
	//10.Un disco de música (en una tienda de música).
	public static class Disco {
		private enum Formato {cd, dvd, vinilo, otros};
		private double precio;
		private int existencias;
		private String titulo;
		private String artista;
		private String estilo;
		private Formato presentacion;
		public Disco(double precio, int existencias, String titulo, String artista, String estilo,
				Formato presentacion) {
			this.precio = precio;
			this.existencias = existencias;
			this.titulo = titulo;
			this.artista = artista;
			this.estilo = estilo;
			this.presentacion = presentacion;
		}
		
	}//class Disco
}
