package practica7;

/**
 * Alarma.java		1 abr. 2019		practica7 ej4y5 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 */
public class Alarma {

	static final double UMBRAL = 3.0;

	private Sensor sensor;
	private Timbre timbre;

	public Alarma(double umbral, Sensor sensor, Timbre timbre) {
		this.sensor = sensor;
		this.timbre = timbre;
	}
	public Alarma() {
		this(3.0, new Sensor(0.0), new Timbre(false));
	}

	public Sensor getSensor() {
		return sensor;
	}

	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}

	public Timbre getTimbre() {
		return timbre;
	}

	public void setTimbre(Timbre timbre) {
		this.timbre = timbre;
	}
	public void comprobar() {
		double sensor = this.getSensor().getValorActual();
		if (sensor >= UMBRAL ) {
			timbre.activar();
		}
		else {
			timbre.desactivar();
		}
	}
	/**
	 * subclase para la luz
	 */
	class AlarmaLuminosa {

		private boolean Estadoluz;		

		public AlarmaLuminosa(boolean estadoluz) {
			Estadoluz = estadoluz;
		}		
		public boolean isEstadoluz() {
			return Estadoluz;
		}
		public void setEstadoluz(boolean estadoluz) {
			Estadoluz = estadoluz;
		}
		public void activarLuz() {

			if ( timbre.isOn() ) {
				setEstadoluz(true);
				new Bombilla(Estadoluz).activar();			
			}
			else {
				setEstadoluz(false);
				new Bombilla(Estadoluz).desactivar();
			}
		}

	}//alarmaLuminosa classs
	private class Bombilla {

		private boolean estado = false;

		public Bombilla(boolean estado) {
			this.estado = estado;
		}

		public boolean isEstado() {
			return estado;
		}

		public void setEstado(boolean estado) {
			this.estado = estado;
		}
		void activar() {
			setEstado(true);
		}
		void desactivar() {
			setEstado(false);
		}

	}//bombilla

}//Alarma class
class Sensor {
	private double valorActual;

	public Sensor(double valorActual) {
		this.valorActual = valorActual;
	}
	public Sensor() {
		this.valorActual = 0.0;
	}

	public double getValorActual() {
		return valorActual;
	}

	public void setValorActual(double valorActual) {
		this.valorActual = valorActual;
	}



}
class Timbre {
	private boolean estado;

	public Timbre(boolean estado) {
		this.estado = estado;
	}

	public boolean isOn() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	void activar() {
		setEstado(true);
	}
	void desactivar() {
		setEstado(false);
	}

}//Timbre class
