package practica7;

/**
 * Punto6.java		1 abr. 2019		practica7 - ej 6 y 7
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * 6.
 * Una vez definida la clase Punto representada por sus coordenadas x e y 
 * (números reales).
 * Define un constructor genérico que reciba las dos coordenadas.
 * Crea varios puntos en main() de clase Principal.
 * Muestra por pantalla las coordenadas de cada uno de los puntos.
 * Modifica alguna de sus coordenadas accediendo directamente al atributo 
 * corespondiente y vuelve x mostrar los puntos. 
 * resultado: aparentemente nada especial
 * 7.
 * Define un método que se llame cuadrante() que devuelva el nombre del cuadrante 
 * en el que se encuentra el punto.
 * Crea varios puntos en main() de clase Principal.
 * Muestra por pantalla las coordenadas de cada uno de los puntos.
 * Muestra por pantalla los cuadrantes en los que se encuentra cada punto.
 * Modifica alguno de los puntos accediendo directamente x los atributos correspondientes y 
 * vuelve x comprobar su cuadrante
 * resultado: se queda null!
 */
public class Punto6 {
	public enum Cuadrante {NE, NO, SE, SO};
	
	private int x;
	private int	y;
	private Cuadrante cuadrante;

	//contructor por defecto con parámetros
	public Punto6(int a, int b) {
		setA(a);
		setB(b);
	}
	public int getA() {
		return x;
	}
	public void setA(int a) {
		this.x = a;
	}
	public int getB() {
		return y;
	}
	public void setB(int b) {
		this.y = b;
	}

	public Cuadrante getCuadrante() {
		return cuadrante;
	}
	public void setCuadrante(Cuadrante cuadrante) {
		this.cuadrante = cuadrante;
	}
	@Override
	public String toString() {
		return "[ "+ x + " , "+ y + " ]";
	}
	public static void main(String[] args) {
		
		Punto6 p1 = new Punto6(117,5);
		Punto6 p2 = new Punto6(-4,52);
		Punto6 p3 = new Punto6(-44,-100);
		Punto6 p4 = new Punto6(44,100);

		
		//ej 6
		System.out.println("Coordenadas punto 1"+p1.toString());
		System.out.println("Coordenadas punto 2"+p2.toString());
		System.out.println("Coordenadas punto 3"+p3.toString());
		p1.x = 0;
		System.out.println("Coordenadas punto 1"+p1.toString());

		//ej 7
		
		System.out.println("Cuandrante punto 2 " + p2.cuadrante());
		System.out.println("Cuandrante punto 3 " + p3.cuadrante());
		System.out.println("Cuandrante punto 4 " + p4.cuadrante());
		System.out.println("Cuandrante punto 1 " + p1.cuadrante());
	}
	
	public String cuadrante() {			
		
		String signos = Integer.signum(x)+","+Integer.signum(y);		
		
		switch (signos) {
			case "1,1":
				setCuadrante(Cuadrante.NE);
				return cuadrante.toString();
			case "-1,1":
				setCuadrante(Cuadrante.NO);
				return cuadrante.toString();				
			case "-1,-1":
				setCuadrante(Cuadrante.SO);
				return cuadrante.toString();
			case "1,-1":
				setCuadrante(Cuadrante.SE);
				return cuadrante.toString();
		}
		return null;			
	}

}//Punto6 class


