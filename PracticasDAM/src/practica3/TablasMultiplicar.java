package practica3;
/**
 * Practica3_124: TablasMultiplicar.java  6 nov. 2018
 * @author Jesús Pérez Robles NRE:4004010  
 *   Escribe un programa que muestre las tablas de multiplicar. Para la visualización utiliza 
 *   un método llamado calcularMostrarTabla().
 *   El método calcularMostrarTabla() recibe el número del que calculará la tabla y la visualiza 
 *   por pantalla con el formato adecuado.
 *   La tabla mostrará productos hasta el 15. 
 *  Deben seguirse los principios y estilo del código limpio.
 */
import java.util.Scanner;
public class TablasMultiplicar {

	static final int CANTIDAD_LIMITE_PRODUCTOS = 15;


	public static void main(String[] args) {
		
		int num_tabla = 0;

		num_tabla = pedir_num_tabla();

		calcularMostrarTabla(num_tabla);
		
	}

	//pedir  y devolver el número para calcular su tabla de multiplicar
	static int pedir_num_tabla() {
		
		System.out.print("Introducir número para mostrar su tabla de multiplicar: ");		 
		return new Scanner(System.in).nextInt();
		
	}

	//calcular tablas    	
	static void calcularMostrarTabla(int multiplicador) { 

		int producto = 0;
		int multiplicando = 0;

		for (multiplicando = 0; multiplicando <= CANTIDAD_LIMITE_PRODUCTOS; multiplicando++) {
			
			producto = multiplicando * multiplicador;
			
			System.out.println(multiplicando + " x " + multiplicador + " = " + producto + "  ");
		} 				
	}

}
