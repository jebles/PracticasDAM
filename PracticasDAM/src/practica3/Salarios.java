package practica3;
/**
 * Practica3_124: Salarios.java  6 nov. 2018
 * @author Jesús Pérez Robles NRE:4004010
 * Programa para calcular los salarios semanales de empleados a los que se les 
 * paga 15 euros por hora hasta el limite de las 35 horas. Cada hora por encima de 35 se 
 * considerará extra y se paga a 22 €. * 
 */
import java.util.Scanner;
public class Salarios {

	//Declaraciones	de las constantes que conforman los parámetros requeridos 
	static int E_POR_HORA = 15;
	static int E_POR_HORA_EXTRA = 22;
	static int LIMITE_HORAS_EXTRA = 35;

	public static void main(String[] args) {

		int horas_trabajadas = 0;
		
		int salario = 0;
		horas_trabajadas = pedir_horas();
		salario = calcularSalario(horas_trabajadas);

		//arrojar resultados
		System.out.println("\nEl salario correspondiente a las "+ horas_trabajadas +
							" horas trabajadas es de "+ salario + " €.");		

	}
	//Cálculos
	static int calcularSalario(int horas_trabajadas) { 	
		
		int salario_total = 0; // variable para el calculo del salario

		if (horas_trabajadas <= LIMITE_HORAS_EXTRA) {		//En caso de horas normales hasta el limite establecido
			salario_total= horas_trabajadas * E_POR_HORA; 	//Calcula el salario total
		} else {											//Si supera el limite de horas
			salario_total = LIMITE_HORAS_EXTRA * E_POR_HORA + 		//Calcula la parte de horas normal
					((horas_trabajadas - LIMITE_HORAS_EXTRA ) * E_POR_HORA_EXTRA); // Y le suma el resto como extras
		}
		return salario_total;
	}

	//pedir datos al trabajador: nº horas	 
	static int pedir_horas() {
		System.out.print("Introducir número de horas trabajadas: ");		 
		return    new Scanner(System.in).nextInt();
	}

}
