package practica3;
/**
 * Practica3_124: Multiplo1.java  6 nov. 2018
 * @author Jesús Pérez Robles NRE:4004010
 * 
 */

import java.util.Scanner;
public class Multiplo4 	{
	
	static int tope;
	public static void main(String argumentos[]) {		
		
		mostrarMultiplos2(pedirTope());				

	}

	static void mostrarMultiplos2(int tope) {
		
		int mult  ;							// Almacena el múltiplo calculado
		int cont  ;							// Contador utilizado en el cálculo

		// Inicializa las variables
		mult = 0 ;
		cont = 0 ;

		System.out.println("\t Múltiplos de 2\n");
		while (mult < tope)					// Bucle de cálculo y visualización
		{
			mult = cont * 2;
			System.out.println("\t  " +    '#' + (cont+1) + '\t' + mult);
			++cont;
		}
	}


	static int pedirTope() {
		System.out.println("Tope: ");		
		return new Scanner(System.in).nextInt();
	}

	
}