
package practica3;

import java.util.Scanner;

/**
 * @author jes
 * método que se llame rombo() que reciba un número entero como parámetro y muestre por pantalla  un rombo de asteriscos.
 */
public class Rombo {

	private static final char ESPACIO = ' ';
	private static final char LADRILLO = '*';
	public static void main(String[] args) {
		
		int num_rombo = 0;
		
		num_rombo = pedir_num();
		
		
		validar_num_impar(num_rombo);
		
		// pintar rombo si el numero es valido
		if (validar_num_impar(num_rombo)) {
			
			construir_piramide_superior((num_rombo + 1) / 2); //recorta la piramide para que cuadre con la arte superior del rombo
			
			construir_piramide_inferior((num_rombo -1) / 2); //dibuja la parte inferior del rombo
			
			
		}	else {
			System.out.print("Error: el número no puede ser par");
			

		}	
		

	}
	private static void construir_piramide_inferior(int num_usuario) {
		int contador_ladrillos = num_usuario;
		int contador_espacios = 1;
		
		for (int j = 0; j < num_usuario ; j++) {			

			for (int k = 0; k <= contador_espacios; k++) {
				System.out.print(ESPACIO); //coloca los ladrillos en cada linea
				
			}
			
			for (int i = 0; i < (contador_ladrillos*2)-1 ; i++) {				
				System.out.print(LADRILLO); //va colocando los espacios descendientemente
			} 
			
			System.out.println(); //cambia de linea			
			contador_ladrillos --; //reduce los cartacteres a imprimir de uno a uno 
			contador_espacios ++; // aumenta los ladrillos a razon de 2 más por linea

		}
	
	}
	private static void construir_piramide_superior(int num_usuario) {
		int contador_caracteres = num_usuario;
		int contador_ladrillos = 1;
		for (int j = num_usuario; j > 0 ; j--) {


			for (int i = contador_caracteres; i > 0; i--) {

				System.out.print(ESPACIO); //va colocando los espacios descendientemente

			} 

			for (int k = 1; k <= contador_ladrillos; k++) {

				System.out.print(LADRILLO); //coloca los ladrillos en cada linea

			}
			System.out.println(); //cambia de linea			
			contador_caracteres --; //reduce los cartacteres a imprimir de uno a uno 
			contador_ladrillos += 2; // aumenta los ladrillos a razon de 2 más por linea

		}
	}
	// comprobar que el numero sea impar por simetria
	private static boolean validar_num_impar(int num_rombo) {
		if (num_rombo%2 == 0) {
			return false;
		}
		return true;
	}
	// pedir numero impar
			static int pedir_num() {

				System.out.print("Introduce un número impar para construir un rombo: ");        
				return new Scanner(System.in).nextInt();

			}	

}
