package practica3;

import java.util.Scanner;

/**
 * Practica3_124: Factorial.java  7 nov. 2018
 * @author Jesús Pérez Robles NRE:4004010
 *
 *   programa que muestre el factorial de un número leído por teclado.
 *   Para el cálculo del factorial utiliza un método llamado factorial() que recibe
 *   el número para el cálculo y devuelve el resultado.
 */

public class Factorial2 {


    public static void main(String[] args) {

        int num_elegido = 0;
        long factorial = 0;
        
        num_elegido = pedir_num();                    // almacena el número de usuario

        factorial = factorial(num_elegido);                // almacena en factorial el resultado del método
        
        System.out.println(num_elegido + "! = " + factorial); //Muestra resultado

    }

    /**
     * Calcula el factorial tomando el @param num_elegido y devuelve el resultado
     */
    
    static long factorial(int numero_usuario) {

        long calculo_fac = 1;

        for (int i = numero_usuario; i > 0; i--) {

            calculo_fac *= i;

        }

        return calculo_fac;
        
    }
    // Pide el numero para hacer el cálculo al usuario
    static int pedir_num() {

        System.out.print("Introduce número para calcular su factorial: ");        
        return new Scanner(System.in).nextInt();

    }

}

