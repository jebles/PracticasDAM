package practica3;

import java.util.Scanner;

/**
 * Piramide.java 15 nov. 2018  practica3 
 * @author Jesús Pérez Robles  NRE4004010 
 * Escribe un método que se llame piramide() que reciba un número entero 
 * como parámetro y muestre por pantalla una pirámide de asteriscos.
 */
public class Piramide {

	static final char LADRILLO = '*';
	static final char ESPACIO = ' ';
	static final int LIM_ALTURA = 20;

	public static void main(String[] args) {

		//declaraciones

		//pide un numero y lo almacena
		int num_usuario = pedir_num();	

		

		// valida que el numero sea entre 1 y el limite establecido
		if (validar_num(num_usuario)) {
			
			construir_piramide(num_usuario);
			
		} else {
			System.out.println("Numero no válido, debe ser desde 1 hasta " + LIM_ALTURA +". \nFin del programa");
		}
	}

	private static void construir_piramide(int num_usuario) {
		int contador_caracteres = num_usuario;
		int contador_ladrillos = 1;
		for (int j = num_usuario; j > 0 ; j--) {


			for (int i = contador_caracteres; i > 0; i--) {

				System.out.print(ESPACIO); //va colocando los espacios descendientemente

			} 

			for (int k = 1; k <= contador_ladrillos; k++) {

				System.out.print(LADRILLO); //coloca los ladrillos en cada linea

			}
			System.out.println(); //cambia de linea			
			contador_caracteres --; //reduce los cartacteres a imprimir de uno a uno 
			contador_ladrillos += 2; // aumenta los ladrillos a razon de 2 más por linea

		}
	}

	private static boolean validar_num(int n) {
		if (n > LIM_ALTURA || n < 1) {
			return false;				
		}
		return true; 
	}

	// Pide el numero para hacer el cálculo al usuario
	static int pedir_num() {

		System.out.print("Introduce un número de plantas para construir la pirámide desde 1 hasta " + LIM_ALTURA + " : ");        
		return new Scanner(System.in).nextInt();

	}

}
