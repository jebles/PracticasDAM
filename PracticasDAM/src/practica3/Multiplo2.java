package practica3;
/**
 * Practica3_124: Multiplo1.java  6 nov. 2018
 * @author Jesús Pérez Robles NRE:4004010
 * 
 */

import java.util.Scanner;
public class Multiplo2 	{
	public static void main(String argumentos[]) {

		Scanner teclado = new Scanner(System.in);

		final int TOPE = 16  ; 

		mostrarMultiplos2(TOPE);

		teclado.close();

	}

	private static void mostrarMultiplos2(int TOPE) {
		// Declaración de variables

		// Constante, el máximo valor del múltiplo
		int mult  ;							// Almacena el múltiplo calculado
		int cont  ;							// Contador utilizado en el cálculo

		// Inicializa las variables
		mult = 0 ;
		cont = 0 ;

		System.out.println("\t Múltiplos de 2\n");
		while (mult < TOPE)					// Bucle de cálculo y visualización
		{
			mult = cont * 2;
			System.out.println("\t  " +    '#' + (cont+1) + '\t' + mult);
			++cont;
		}

	}
}