package practica3;

import java.util.Scanner;

/**
 * Practica3_124: Factorial.java  7 nov. 2018
 * @author Jesús Pérez Robles NRE:4004010
 *
 *   programa que muestre el factorial de un número leído por teclado.
 *   Para el cálculo del factorial utiliza un método llamado factorial() que recibe
 *   el número para el cálculo y devuelve el resultado.
 */

public class Factorial {

	static final int RANGO_MAX = 20;	//Se elige este rango para que el resultado no exceda el rango de un long
	static final int RANGO_MIN = -20;

	public static void main(String[] args) {

		int num_elegido = 0;
		long factorial = 0;

		num_elegido = pedir_num(); // almacena el numero de usuario   

		while (!validar_num(num_elegido)) {	// Valida que el numero esté dentro del rango posible

			System.out.println("El numero tiene que estar dentro del rango "+ RANGO_MIN +" a 20"+ RANGO_MAX +"\n");

			num_elegido = pedir_num(); 

			validar_num(num_elegido);

		}        

		factorial = factorial(num_elegido);                // almacena en factorial el resultado del metodo

		System.out.println(num_elegido + "! = " + factorial); //Muestra resultado





	}


	//Calcula el factorial tomando el @param num_elegido y devuelve el resultado
	static long factorial(int numero_usuario) {

		long resultado_fac = 1;

		//Calculo para un número positivo o 0
		if (numero_usuario > -1 ) {    

			for (int i = numero_usuario; i > 0; i--) { //Si es cero, me devuleve 1, que es el resultado real de 0!

				resultado_fac *= i;

			}

			//Caso para un numero negativo, sistema: -6! = -(|6|!) = -720
		} else {    

			numero_usuario = Math.abs(numero_usuario); //Valor absoluto del número para el cálculo,

			for (int i = numero_usuario; i > 0; i--) { //cálculo idéntico al positivo

				resultado_fac *= i;

			} resultado_fac = Math.negateExact(resultado_fac); // Pasa a negativo el resultado                

		}

		return resultado_fac;

	}

	// Pide el numero para hacer el cálculo al usuario
	static int pedir_num() {

		System.out.print("Introduce un número para calcular su factorial entre -20 y 20: ");        
		return new Scanner(System.in).nextInt();

	}

	//Metodo para validar que el número cumple los requisitos
	static boolean validar_num(int num_elegido) {    

		if (num_elegido < -20 || num_elegido > 20) {

			return false;

		} return true;
	}

}
