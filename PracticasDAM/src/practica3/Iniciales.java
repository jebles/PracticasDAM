package practica3;

/**
 * Iniciales.java 15 nov. 2018  practica3 
 * @author Jesús Pérez Robles  NRE4004010 
 * método que se llame banner() que muestra tus iniciales en la pantalla. 
 * Cada inicial se compone sobre una celda en pantalla de 7x7
 */
public class Iniciales {


	static final int SIZE = 7;
	static final char PRIMERA_LETRA = 'J';
	static final char SEGUNDA_LETRA = 'P';
	static final char TERCERA_LETRA = 'R';

	static final char ESPACIO = ' ';

	public static void main(String[] args) {		


		/* Cada letra esta formada por 7 lineas de caracteres que forman un patrón por linea y por letra. 
		 * Dependiendo de la letra que esté formando, va apilando los patrones por cada linea.
		 * Hay un total de 4 patrones.
		 */

	
		//repetimos el patron las veces necesarias:
		
		// primera linea de cada letra separada por espacios		
		patron_A(PRIMERA_LETRA); System.out.print(ESPACIO); patron_C(SEGUNDA_LETRA);System.out.print(ESPACIO); patron_C(TERCERA_LETRA); System.out.println();
		//segunda linea
		patron_A(PRIMERA_LETRA); System.out.print(ESPACIO); patron_B(SEGUNDA_LETRA);System.out.print(ESPACIO); patron_B(TERCERA_LETRA); System.out.println();
		//3ª linea
		patron_A(PRIMERA_LETRA); System.out.print(ESPACIO); patron_B(SEGUNDA_LETRA);System.out.print(ESPACIO); patron_B(TERCERA_LETRA); System.out.println();
		//4ª linea
		patron_A(PRIMERA_LETRA); System.out.print(ESPACIO); patron_C(SEGUNDA_LETRA);System.out.print(ESPACIO); patron_C(TERCERA_LETRA); System.out.println();
		//5º linea
		patron_A(PRIMERA_LETRA); System.out.print(ESPACIO); patron_D(SEGUNDA_LETRA);System.out.print(ESPACIO); patron_B(TERCERA_LETRA); System.out.println();
		//6ª linea
		patron_B(PRIMERA_LETRA); System.out.print(ESPACIO); patron_D(SEGUNDA_LETRA);System.out.print(ESPACIO); patron_B(TERCERA_LETRA); System.out.println();
		//7ª linea
		patron_C(PRIMERA_LETRA); System.out.print(ESPACIO); patron_D(SEGUNDA_LETRA);System.out.print(ESPACIO); patron_B(TERCERA_LETRA); System.out.println();

		
		

		


	}
	//cada linea de texto es un patrón
	private static void patron_A(char letra) {

		//esta variable indica el numero de veces que hay una letra en cada linea y letra
		int num_letras = 1;

		//hace un bucle de espacios hasta el tamaño menos el numero de letras que va a introducir
		for (int i = 0 ; i < SIZE - num_letras; i++) {
			System.out.print(ESPACIO);

			//cuando llega a la posicion 4 introduce la letra
			if (i == 4) {
				System.out.print(letra);				
			} 
		}
	}
	private static void patron_C(char letra) {

		int num_letras = 4;			

			System.out.print(ESPACIO);		
		
		for (int i = 0; i < num_letras; i++) {
			System.out.print(letra);
		}
			System.out.print(ESPACIO);
			System.out.print(ESPACIO);
	}
	private static void patron_D(char letra) {
		
		int num_letras = 1;
		for (int i = 0 ; i < SIZE - num_letras; i++) {
			System.out.print(ESPACIO);

			if (i == 0) {

				System.out.print(letra);

			} 
		}
	}

	private static void patron_B(char letra) {
		int num_letras = 2;
		for (int i = 0 ; i < SIZE - num_letras; i++) {
			System.out.print(ESPACIO);

			if (i == 3 || i == 0) {

				System.out.print(letra);

			} 
		}
	}
}
