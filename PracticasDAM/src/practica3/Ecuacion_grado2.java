package practica3;
import java.util.Scanner;

/**
 * @author JPR NRE4004010
 * Escribe un programa simple que pida tres valores reales a, b, c 
 * Calcule las soluciones reales X1 y X2 de una ecuación de segundo grado de la forma ax2 + bx + c = 0.
 * Las soluciones de la ecuación vienen dadas por:
 * X1 = (-b + sqrt(b2 - 4ac)) / (2a)
 * X2 = (-b - sqrt(b2 - 4ac)) / (2a)
 * 
 */
public class Ecuacion_grado2 {

	static double discriminante = 0; 
	
	public static void main(String[] args) {
		
		double x1 = 0;
		double x2 = 0;
		double a = tomar_valor_coeficiente("a");
		double b = tomar_valor_coeficiente("b");
		double c = tomar_valor_coeficiente("c");
		
		int codigo_evaluacion = evaluarDiscriminante(a, b, c);

		switch (codigo_evaluacion) {
		case 1: 
			// ec degenerada
			System.out.println("La ecuación es degenerada");
			break;
		case 2:  
			x1 = c/b;
			System.out.println("Existe una raíz única con valor " + x1);
			break;
		case 3:
			x1 = ((-b + Math.sqrt(discriminante))/2*a);
			x2 = ((-b - Math.sqrt(discriminante))/2*a);
			System.out.println("La ecuacion tiene dos soluciones: \nx1 = " + x1 + "\nx2 = " + x2);
			break;
		case 4:			
			double x = -b/2 ;
			double y = (Math.sqrt(Math.abs(discriminante)))/2;
			System.out.print(" la ecuación tiene dos raíces complejas:\n" + x +" + " + y + "i\n"
																		  + x +" - " + y + "i");
			break;		
		}
		System.out.println(codigo_evaluacion);


	}

	static int evaluarDiscriminante(double a, double b, double c) {
			

		discriminante = Math.pow(b,2)-4*a*c;	
		
		int codigo_resultado = 0;	
		
		
		
	
		//validacion de datos introducidos
		if (a == 0 && b == 0) {
			codigo_resultado = 1;
		} else if (a == 0 && b!=0) {
			codigo_resultado = 2;
//			discriminante = -c/b;
		} else if (discriminante >= 0) {
			codigo_resultado = 3;
			
		} else {
			codigo_resultado = 4;
		}

		return codigo_resultado;
	}
	public static int tomar_valor_coeficiente(String coeficiente) {
		System.out.print("Por favor, introduzca el valor del coeficiente " + coeficiente + " : ");
		return new Scanner(System.in).nextInt(); 
	}
	

}

