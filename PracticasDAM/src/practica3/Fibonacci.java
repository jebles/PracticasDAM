package practica3;
/**
 *  	
    Escribe un método llamado fibonacci() que reciba un entero, compruebe que es válido y devuelva el valor 
    que le corresponde en la sucesión de Fibonacci.
    El método fibonacci() calcula su valor de la siguiente manera:
        Sólo debe admitir números positivos
        Si el número recibido es un 0, el termino de la sucesión vale 0; si es un 1, la sucesión vale 1;  
        si es un 2, la sucesión vale 1, si es un 3, la sucesión vale 2...

		0, 1, 1, 2, 3, 5...

        Para los sucesivos términos, se obtienen sumando los dos términos anteriores.
        Por ejemplo, para el término 5 el valor es la suma del término para n=4 más el término para n=3.
    Prueba el método pedido desde main().
 *
 */
import java.util.Scanner;
public class Fibonacci {

	static final int MAX_VALOR = 55; //puesto este limite para que no se salga de los limites de int
	static final int MIN_VALOR = 0;


	public static void main(String[] args) {


		int num_usuario = 1; 

		//pide un numero y lo almacena en bucle hasta que se introduzca uno valido

		do  {
			num_usuario = pedir_num();	
		} while (num_usuario > MAX_VALOR || num_usuario < MIN_VALOR);
		
		System.out.println("La "+ num_usuario + "ª posicion en la secuencia corresponde a: " + fibonacci(num_usuario));		

	}

	private static int fibonacci(int num) {

		assert num > MAX_VALOR : "Numero incorrecto" ;
		assert num < MIN_VALOR : "Numero incorrecto" ;

		int n = 0;
		int n_1 = 1;
		int n_2 = 0;		

		for (int i = 1; i < num; i++) {

			n = n_1 + n_2;
			n_2 = n_1;
			n_1 = n;			

		} return n;
	}


	static int pedir_num() {
		System.out.println("Por favor, introduce un número entre "+ MIN_VALOR + " y " + MAX_VALOR + " para saber el valor que corresponde a su posicion en la sucesión de Fibonacci: ");
		return new Scanner(System.in).nextInt();
	}	


}
