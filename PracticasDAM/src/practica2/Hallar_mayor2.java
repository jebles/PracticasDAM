package practica2;
/**
 * @author dam18-23 4004010
 * programa simple que lea una serie de números enteros por teclado e 
 * indique cuál es el mayor de todos ellos
 */
import java.util.Scanner;
public class Hallar_mayor2 {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);		
		String continuar = "";
		int mayor=0;
		int num1 = 0;
		int num2 = 0;
		while (continuar != "s") {

			System.out.print("Introduce un número..........");
			num1 = teclado.nextInt();
			System.out.print("Introduce un número..........");
			num2 = teclado.nextInt();

			if (num1>num2) {
				mayor = num1;
			} 
			else {
				mayor = num2;
			}
			System.out.println("El mayor es "+ mayor);
			teclado.next();
			System.out.println("s para seguir");			
			continuar = teclado.next().toLowerCase();
		}
		System.out.println("Fin del programa");
	}
}
