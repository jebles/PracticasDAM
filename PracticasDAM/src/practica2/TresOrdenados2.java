package practica2;
/**
 * @author dam18-23
 * programa simple que pide tres datos de tipo entero por teclado, 
 * los guarda y los muestra ordenados por pantalla
 */
import java.util.Scanner;
public class TresOrdenados2 {
	
	public static void main(String[] args) {

		//declaraciones
		Scanner teclado = new Scanner(System.in);
		
		//Pedir datos
		System.out.print("Introduce el primer número..........");
		int num1 = teclado.nextInt();
		System.out.print("Introduce el segundo numero.........");
		int num2 = teclado.nextInt();
		System.out.print("Introduce el tercer número..........");
		int num3 = teclado.nextInt();
		
		
		//Evaluar datos
		int menorA = Math.min(num2, num3);
		int menor = Math.min(menorA, num1);
		int mayorA = Math.max(num1, num2);
		int mayor = Math.max(mayorA, num3);
		
		
		
		//Mostrar resultados
		System.out.println(menor);
		System.out.println(mayor);
		System.out.println("");

	}

}
