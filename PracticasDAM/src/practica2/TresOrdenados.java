package practica2;
/**
 * @author dam18-23
 * programa simple que pide tres datos de tipo entero por teclado, 
 * los guarda y los muestra ordenados por pantalla
 */
import java.util.Scanner;
public class TresOrdenados {
	
	public static void main(String[] args) {

		//declaraciones
		Scanner teclado = new Scanner(System.in);
		
		//Pedir datos
		System.out.print("Introduce el primer número..........");
		int num1 = teclado.nextInt();
		System.out.print("Introduce el segundo numero.........");
		int num2 = teclado.nextInt();
		System.out.print("Introduce el tercer número..........");
		int num3 = teclado.nextInt();
		
		
		//Evaluar datos
		if (num1 > num2) {
			int aux = num1; //usar una variable intermedia para intercambiar datos
			num1 = num2; //
			num2 = aux; //
		}
		if (num1 > num3) {
			int aux = num1; //usar una variable intermedia para intercambiar datos
			num1 = num3;
			num3 = aux;
		} //hasta aqui se asegura que el numero 1 es el mas pequeño
		if (num2 > num3) {
			int aux = num2; //usar una variable intermedia para intercambiar datos
			num2 = num3;
			num3 = aux;
		}
		
		//Mostrar resultados
		System.out.println(num1);
		System.out.println(num2);
		System.out.println(num3);

	}

}
