package practica2;
/**
 * @author dam18-23 - 4004010
 *  programa simple que calcule el interés producido y el capital total acumulado 
 *  de un capital inicial invertido a un tipo de interés anual.
 */
import java.util.Scanner;
public class Hallar_interes {

	public static void main(String[] args) {
		//pedir datos
		Scanner teclado = new Scanner(System.in);
		
		System.out.print("Introduce el Capital inicial..........");
		int Ci = teclado.nextInt();
		System.out.print("Introduce el tipo de interés anual....");
		int r = teclado.nextInt();
		System.out.print("Introduce el número de años...........");
		int n = teclado.nextInt();
		
		//hacer los calculos 
		/*
		Cf = Ci * (1 + r)n
	    Cf es el capital final.
	    Ci es el capital inicial.
	    r es el interés anual en tanto por uno.
	    n es el número de años.		
		*/
		
		double Cf = Ci * Math.pow((1+r), n);		
		
		
		//mostrar resultados
		//System.out.println("CAPITAL INICIAL="+Ci+"INTERES="+r+"AÑOS="+n);
		System.out.println("\nEl capital final es " + Cf);

	}

}
