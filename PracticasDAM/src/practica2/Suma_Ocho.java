package practica2;
/**
 * @author dam18-23 4004010
 * programa simple que lee exactamente ocho números enteros y luego escribe la suma de todos ellos.
 */
import java.util.Scanner;

public class Suma_Ocho {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		
		System.out.print("Introduce un número..........");
		int num = teclado.nextInt();
		int i = 0;
		
		while (i<7) {
			System.out.print("Introduce otro número........");
			num = num + teclado.nextInt();
			i++;
		}
		
		System.out.println("La suma de todos los números introducidos es "+num);

	}

}

