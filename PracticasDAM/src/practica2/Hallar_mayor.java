package practica2;
/**
 * @author dam18-23 4004010
 * programa simple que lea una serie de números enteros por teclado e 
 * indique cuál es el mayor de todos ellos
 */
import java.util.Scanner;
public class Hallar_mayor {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);		
		
		System.out.print("Introduce un número..........");
		int num1 = teclado.nextInt();
		String salir = null;
		int num2 = 0;
		int mayor;
		
		while (salir != "s") {
			
			System.out.print("Introduce otro número..........");
			num2 = teclado.nextInt();
				if (num1>num2) {
					mayor = num1;
				} else {
					mayor = num2;
				}
			System.out.println("El mayor es "+mayor+"\n");
			System.out.println("Pulse S para seguir comparando, cualquier otra para salir........");
			salir = teclado.nextLine();
		}
		

	}

}
