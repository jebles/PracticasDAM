package practica1;
/**
 * Saludos.java
 * Programa simple que pide y visualice el nombre y la edad 
 * de dos personas en dos líneas diferentes.
 * JPR - 4004014 6.10.18
 */

import java.util.Scanner;

public class Nombre1 {
	public static void main(String[] args) {
		/* teclado es un canal de entrada por teclado a través de un Scanner.
		   Necesita importar el paquete java.util
		 */
		Scanner teclado = new Scanner(System.in); 
		//Declara las variables que van a contener los datos del usuario 1
		String nom1;
		byte edad1;
		//Pide al usuario que introduca su nombre
		System.out.println("Hola usuario 1. Escribe tu nombre");
		//recoge el dato a la variable creada para contenerlo
		nom1 = teclado.nextLine();
		//Pide al usuario que introduca su edad
				System.out.println("ahora escribe tu edad");
				//almacena el dato en la variable
				edad1 = teclado.nextByte();
		System.out.print("Hola usuario 1. Te llamas "+nom1+ " y tienes "+edad1+" años,"
				+ " ¡aprovecha el momento!");
	
		
	//Declara las variables que van a contener los datos del usuario 2
	String nom2;
	byte edad2;
	//Pide al usuario que introduca su nombre
	System.out.println("Hola usuario 2. Escribe tu nombre");
	//recoge el dato a la variable creada para contenerlo
		nom2 = teclado.nextLine();
	//Pide al usuario que introduca su edad
			System.out.println("ahora escribe tu edad");
			//almacena el dato en la variable
			edad2 = teclado.nextByte();
			System.out.print("Hola usuario 2. Te llamas "+nom2+ " y tienes "+edad2+" años,"
					+ " ¡aprovecha el momento!");
	}
}