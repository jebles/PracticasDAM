package practica1;
/**
 * Holamundo.java
 * @author jes 4004010 JPR
 * Programa sencillo que muestra un texto en la consola
 */
public class Holamundo {
	public static void main(String argumentos[]) {	//inicia el metodo principal
		//Muestra el mensaje de dentro en la pantalla
		System.out.print("Hola mundo"); 
		System.out.print("\n" //muestra el autor y el nombre de programa en lineas separadas
				+ "Holamundo.java\n" + 
				"@author jes 4004010 JPR\n" + 
				"Programa sencillo que muestra un texto en la consola");
	}
}

