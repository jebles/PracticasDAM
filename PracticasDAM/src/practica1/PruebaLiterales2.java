package practica1;
/**
 * PruebaLiterales2.java
 * envoltorios
 * Programa que crea wrappers y los saca por consola
 * JPR 4004010  6.10.18
 */

public class PruebaLiterales2 {
	
	public static void main(String[] args) {	
	//inicia y crea las variables váildas del ejercicio anterior como objetos: wrappers 
		Double v1 = -11.1;
		Double v2 = .3e27;
		String v4 = "cañón";
		Integer v5 = 0377;
		Integer v6 = 9999;
		Double v8 = +521.6;
		Double v9 = 1.26;
		Double v10= 5E-002;
		Short v11 = 0xFE;
		Byte v12 = 0b101010;
		Float v13 = 1.26f;
		Character v14  = '\n';
		Integer v17 = 1234;
		Double v18 = .567;
		Integer v19=0xFFFE;
		String v21 = "a";
		Double v22  = 02.45;
		Character v23 = 'a';
		Integer v24 = 0xf;
		String v28 = "abc'";
		String v30 =  "abc;";
		String v32 = "True"; 
		Boolean v34 = false;
		Character v35 = '\\';
		
		//Saca todas por consola
		System.out.println(v1);
		System.out.println(v1);
		System.out.println(v1);
		System.out.println(v1);
		System.out.println(v1);
		System.out.println(v1);
		System.out.println(v2);
		System.out.println(v4);
		System.out.println(v5);
		System.out.println(v6);
		System.out.println(v8);
		System.out.println(v10);
		System.out.println(v11);
		System.out.println(v13);
		System.out.println(v14);
		System.out.println(v17);
		System.out.println(v18);
		System.out.println(v19);
		System.out.println(v21);
		System.out.println(v22);
		System.out.println(v23);
		System.out.println(v24);
		System.out.println(v28);
		System.out.println(v30);
		System.out.println(v32);
		System.out.println(v34);
		System.out.println(v35);		
		
	}
}