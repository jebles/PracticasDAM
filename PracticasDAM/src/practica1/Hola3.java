package practica1;
/**
 * @author dam18-23 4004010 JPR
 * Práctica  1.5.1 comentada
 */

// importa la herramienta de lectura de datos del teclado
import java.util.Scanner;


public class Hola3
{
    public static void main(String argumentos[]) {
        
        // Crea la variable teclado para almacenar los datos que se introducen por teclado
        @SuppressWarnings("resource")
		Scanner teclado = new Scanner(System.in);
        
        // Crea una variable de tipo int para almacenar números
        int num;     
        // 
       
        // Le el valor 1 y a continuación lo saca por pantalla con un texo 
        num = 1;
        System.out.println("Hola, Soy un modesto ");
        System.out.print("programa de ordenador...\n");
        System.out.println("Mi número de orden es el " + num + " por ser el primero.");

        // Pide al usuario que intriduzca 2 numeros y recoge los datos en dos variables tipo int
        System.out.println("Escribe dos números...\n");
        int dato1 = teclado.nextInt();
        int dato2 = teclado.nextInt();

        // Muestra en la consola los numeros introducidos
        System.out.println("Dato1: " + dato1);
        System.out.println("Dato2: " + dato2);

        /** Crea una variable de cadena de texto y le da valor que toma de una serie
          * de condiciones mediante una secuencia de sentencias if que comparan las
          * dos variables introducidas por el usuario y devuelven el resultado de la 
          * comparación. El objetivo es sacar el mayor (o igualdad)
        */
        String mensaje = "";

        if (dato1 == dato2) {
            mensaje = "Ninguno de los dos es mayor... ";
        }

        if (dato1 > dato2) {
            mensaje = "El mayor es: " + dato1;
        }

        if (dato2 > dato1) {
            mensaje = "El mayor es: " + dato2;
        }

        System.out.println(mensaje);
    }

}