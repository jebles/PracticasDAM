package practica1;
/**
 * Saludos.java
 * Programa simple que muestra varios mensajes en la consola del sistema.
 * ajp - 2018.09.19
 */

import java.util.Scanner;

public class Saludos { //comienza la clase Saludos como publica (accesible desde fuera)
	public static void main(String[] args) {
		/* teclado es un canal de entrada por teclado a través de un Scanner.
		   Necesita importar el paquete java.util
		 */
		@SuppressWarnings({ "unused", "resource" })
		Scanner teclado = new Scanner(System.in); //la variable teclado recogerá el dato
		//que introduzca el usuario

		int numOrden;			// Declara una variable numérica, no le da valor.
		@SuppressWarnings("unused")
		String nombre = "";		// Declara de una variable de texto vacía. 

		// Configura y muestra mensajes de bienvenida.
		numOrden = 1 ; //da el valor 1 a la variable numOrden
		//Da la bienvenida y las intrucciones a seguir al usuario
		System.out.println("Hola,"); 
		System.out.print("Soy un modesto ");
		System.out.print("programa de ordenador...\n");
		System.out.println("Bienvenido al Curso.\t" + "Este es el saludo nº " + numOrden);
		//aquí incrementa el valor de la variable nomOrden en 1
		numOrden++;
		System.out.println("Bienvenido a Java.\t" + "Este es el saludo nº " + numOrden);
		System.out.println("\nFin programa..."); 
	}
}