package practica1;
/**
 * PruebaLiterales.java
 * JPR 4004010  6.10.18
 */

public class PruebaLiterales {
	
	public static void main(String[] args) {	
		double v1 = -11.1;
		double v2 = .3e27;
		//double v3 = 23e2.3; No permite el marcador decimal en esa poscion
		String v4 = "cañón";
		int v5 = 0377;
		int v6 = 9999;
		//long v7 = 099; out of range con todos los tpos numéricos
		double v8 = +521.6;
		double v9 = 1.26;
		double v10= 5E-002;
		short v11 = 0xFE;
		byte v12 = 0b101010;
		float v13 = 1.26f;
		char v14  = '\n';
		//String v15 = while; es un apalabra reservada
		//String v16 = \xFE; no es numero ni está introducido correcto como char ni String
		int v17 = 1234;
		double v18 = .567;
		int v19=0xFFFE;
		//char v20 = XGA; No está delimitado para ser String valido
		String v21 = "a";
		//String v22 = 'abc'; demasiados caracteres para char, incorrecta para string 
		double v22  = 02.45;
		char v23 = 'a';
		int v24 = 0xf;
		//char v25 = abc; no puede ir sin ""
		//String v25 = ab"c; mala posicion de "
		//String v26 = "abc;mala posicion de "
		//String v27 = 		"abc'; Missing quote mala posicion de "
		String v28 = "abc'";
		//char v29 = a'; le falta el inicio de ' 
		String v30 =  "abc;";
		//String v31 = "abc""; mala posicion de " 
		String v32 = "True"; 
		//boolean v33 = True; ha de ir en minuscula para ser booleano 
		boolean v34 = false;
		char v35 = '\\';
		
	}
}