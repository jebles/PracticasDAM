package practica1;
/**
 * Pruebaidentificadores.java
 * comprobación de nombres válidos e inválidos para identificadores
 * JPR 4004010  6.10.18
 */

public class PruebaIdentificadores {
	public static void main(String[] args) {	
		int _alpha; //Valido
		String FLOAT; //Valido
		//double 1_de_muchos; no valido por empezar con numero
		int maxValor; //Valido
		byte cuantos; //Valido
		//String "dado"; no valido por empezar con simbolo distinto a _
		//String ""; no valido por empezar con simbolo distinto a _
		double Nbytes; //Valido
		//int pink.panter; No puede contener un .
		//int int; No puede ser una palabra reservada
		//boolean qué_dices?; contiene ? que es un caracter inválido
		double Número; //Valido
		//int cadena 2; no puede haber una espacio
		boolean Cañón; //válido
		String Café; //válido
		//int Mesa-3; no es valido el caracter -
		double Return; //valido porque empieza por mayuscula a diferencia de la palabra reservada
		String While; //valido porque empieza por mayuscula a diferencia de la palabra reservada
		byte __if; //válido
		//int Bloque#4; caracter # inválido
		String _CaPrIcHoSo_; //Valido
		//char 8ºRoca; contiene un caracter invalido: ª y empieza por número
		//int 3d2; no puede empezar por numero
		//long Hoja3/5; / es un caracter inválido  	
	}
}