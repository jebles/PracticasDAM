package practica5;

import java.text.StringCharacterIterator;

/**
 * Parentesis_2.java		10 ene. 2019		practica5 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * método que se parentesisCorrectos() que recibe una cadenas de texto que contiene una expresión 
 * aritmética en la que hay que comprobar que los paréntesis están bien emparejados. 
 * Devuelve verdadero o falso.    Ejemplo:
 * Expresión: ((a + b) / 5-d) Daría: true
 * Expresión: )(a + b)) Daría: false
 * Utiliza un contador para los paréntesis: Cuando se abre un paréntesis incrementa un contador, 
 * cuando se cierra se decrementa. Al final el contador debe valer 0; en cualquier otro caso la expresión es incorrecta.
 */
public class Parentesis_2 {

	public static void main(String[] args) {
		
		String expr1 = "((a + b) / 5-d)";
		String expr2 = ")(a + b))";

		System.out.println(parentesisCorrectos(expr1));

	}
	/**
	 * El método cuenta +1 cada "(" y -1 cada ")"
	 * @param expresion
	 * @return true si el orden y cantidad de paréntesis son las correctas
	 */
	private static boolean parentesisCorrectos(String expresion) {
		
		int contador = 0; 
		
		for (int i = 0; i < expresion.length(); i++) {			
			
			if (expresion.charAt(i) == '(') {
				contador++;
			}
			if (expresion.charAt(i) == ')') {
				if (contador > 0) { //si hay un ( antes
					contador--;
				}
				else {
					return false; //si empieza por ) es falso todo lo que venga
				}
			}
		
		}		
		return (contador == 0);
	}
}
