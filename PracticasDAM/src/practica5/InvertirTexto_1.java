package practica5;

/**
 * InvertirTexto_1.java		10 ene. 2019		practica5 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * Método que se llame invertirTexto() que recibe una cadena de caracteres cualquiera y la devuelve invertida.
 * "Introducción" -> "nóiccudortnI" 
 * Se recomienda utilizar internamente un StringBuilder y un bucle do-while 
 */
public class InvertirTexto_1 {

	public static void main(String[] args) {
		
		String texto = "InvertirTexto_1";		 
		
		System.out.println(invertirTexto(texto));

	}
	/**
	 * invierte una cadena de caracteres
	 * @param texto 
	 * @return texto invertido
	 */
	private static String invertirTexto(String textoInicial) {
		
		StringBuilder output =  new StringBuilder(textoInicial);
		
		for (int i = 0, j = textoInicial.length()-1; i < textoInicial.length(); i++, j--) {			
					
			output.setCharAt(j, textoInicial.charAt(i));//solo funciona en la primera iteracion
			output.setCharAt(i, textoInicial.charAt(j));			
		}		
		return output.toString();		
	}
}
