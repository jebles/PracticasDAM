package practica5;

/**
 * Subcadena_3.java		10 ene. 2019		practica5 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * método obtenerNumeroVecesSubCadena() que recibe dos cadenas y devuelve el número de veces que la segunda cadena está contenida 
 * en el texto de la primera teniendo en cuenta las mayusculas y minusculas
 *     Ejemplo: Si se busca la subcadena "en" en el texto:  
 *     “Estamos viviendo en un submarino amarillo. No tenemos nada que hacer. En el interior del submarino se está muy apretado. 
 *     Así que estamos leyendo todo el día. Vamos a salir en 5 días".
 *     Daría: 6
 */
public class Subcadena_4 {

	public static void main(String[] args) {
		
		String cadenaA = "Estamos viviendo en un submarino amarillo. No tenemos nada que hacer. "
						+ "En el interior del submarino se está muy apretado. "
						+ "Así que estamos leyendo todo el día. Vamos a salir en 5 días";
		String cadenaB = "en";
		
		System.out.println(obtenerNumeroVecesSubCadena(cadenaA, cadenaB));
	}
	/**
	 * Método que recorre el String A para buscar el contenido del String B dentro.
	 * @param cadenaA
	 * @param cadenaB
	 * @return numero de coincidencias 
	 */
	private static int obtenerNumeroVecesSubCadena(String A, String B) {
		int contador = 0; //contador de coincidencias del String B completo dentro de A
		int j = 0; //contador de recorrido del String B 
		int y = 0; //contador auxiliar de recorrido del String A
		int x = 0; //contador auxiliar del recorrido del String B
		
		//pasa los String a un mismo case para no discriminar
		A = A.toLowerCase();
		B = B.toLowerCase();
		//recorre A con i
		for (int i = 0; i < A.length()-1; i++) {
			
			if (A.charAt(i) == B.charAt(j)) {
				x = i;
				y = j;
				while (A.charAt(x) == B.charAt(y)) {
					
					if (y == B.length()-1) {
						contador++;
						break;
					}
					x++;
					y++;
				}		
			}
			j = 0; //reinicia j
		}
		return contador;
	}
}
