package practica5;

/**
 * etiquetas_5.java		31 ene. 2019		practica5 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * Método que se llame PasarMayúsculaSubCadena() que recibe un texto etiquetado y devuelve otra cadena en la que se han cambiado
 * a mayúsculas todos los caracteres en el texto entre las etiquetas <mayus></mayus> 
 * Las etiquetas no se pueden anidar y deben ser limpiadas del texto resultante.
 *     Ejemplo: “Estamos viviendo en un <mayus>submarino amarillo</mayus>. No tenemos <mayus>nada</mayus> qué hacer"
 *     Daría: “Estamos viviendo en un SUBMARINO AMARILLO. No tenemos NADA qué hacer”
 * Se recomienda utilizar expresiones regulares o IndexOf() para abrir y cerrar la etiqueta. 
 * Una vez calculado el índice de inicio y final del texto afectado por una etiqueta se extrae, se pasa a mayúscula se reemplaza toda la subcadena <mayus>texto</mayus>.
 */
public class etiquetas_5 {

	public static void main(String[] args) {
		
		String texto = "Estamos viviendo en un <mayus>submarino amarillo</mayus>. No tenemos <mayus>nada</mayus> qué hacer";
		
		System.out.println("\n"+PasarMayusculaSubCadena(texto));

	}
	/**
	 * Método que recibe un texto etiquetado y devuelve otra cadena en la que se han cambiado a mayúsculas 
	 * todos los caracteres en el texto entre las etiquetas <mayus></mayus>
	 * @param texto
	 * @returns texto procesado
	 * Las etiquetas no se pueden anidar y deben ser limpiadas del texto resultante.
	 */
	private static StringBuilder PasarMayusculaSubCadena(String textoCrudo) {
		
		String[] etq = textoCrudo.split("<mayus>|</mayus>");
		
		for (String textMay: etq) {
			etq.toString().toUpperCase();
		}
		
		
		
		StringBuilder result =  new StringBuilder();
		
		
		for (int i = 0; i < textoCrudo.length(); i++) {
			
							
	
		}
		return result;
		
	}
	

}
