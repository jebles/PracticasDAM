package practica5;

/**
 * Encriptar_9.java		11 ene. 2019		practica5 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * método que se llame encriptar() que reciba un texto y una clave. 
 * Devuelve el texto encriptado en otra cadena. 
 * El cifrado debe hacerse aplicando XOR entre cada letra del texto y una letra de la clave, 
 * cuando se terminan las letras de la clave vuelve a la primera.
 * Utiliza la longitud de la clave, clave.length(). 
 * Para mapear cada carácter del texto a encriptar con un carácter de la clave, 
 * calcula el módulo indice % clave.length(). 
 * Se puede realizar la operación XOR binaria entre caracteres convirtiéndolos en números de tipo short. 
 */
public class Encriptar_9 {

	public static void main(String[] args) {
		String key = "8p452";

		String text = encriptar("El cifrado debe hacerse aplicando XOR", key);//encripto
		System.out.println(text);//imprime lo encriptado
		System.out.println(encriptar(text, key));// desencriptar
	}
	/**
	 * Metodo que encripta y desencripta lo encriptado según una clave
	 * @param texto a encriptar
	 * @param clave base para la encriptacion
	 * @returns texto modificado 
	 */
	
	private static String encriptar(String text, String key) {
		StringBuilder result =  new StringBuilder();
		
		for (int i = 0; i < text.length(); i++) { 
						//el casteo a char pasa los numeros resultantes a caracteres
			result.append((char)(text.charAt(i) ^ key.charAt(i % key.length()))); // ^es el operador XOR en BINARIO, que es como trabaja
		}
		return result.toString();
	}

}
