package practica8;

import java.util.ArrayList;

/**
 * Controlador.java		16 abr. 2019		practica8 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * clase para implementar los metodos auxiliares de operaciones
 */
public class Controlador {

	//Singelton
	private static Controlador instance;

	public static Controlador getControlador() {

		if (instance == null) {
			instance = new Controlador();
		}
		return instance;

	}

	public ArrayList<Estudiante> cargarEstudiantesPrueba() {

		ArrayList<Estudiante> listaEstudiantes = new ArrayList();		
		for (int i = 0; i < 10; i++) {
			Estudiante est = new Estudiante(
					"nom"+i, 
					"ape"+i, 
					(Math.rint(Math.random()*100)/10)
					);	
			listaEstudiantes.add(est);
			System.out.println(listaEstudiantes.get(i).toString());
		}		
		return listaEstudiantes;
	}
	public ArrayList<Estudiante> quickSortEvaluacionEstudiantes(ArrayList<Estudiante> listaEs, int izq, int der) {

		int pivote = (izq + der) / 2;
		Estudiante aux;
		int i = izq;
		int j = der;
		int comparacionI = listaEs.get(i).compareTo(listaEs.get(pivote));
		int comparacionJ = listaEs.get(j).compareTo(listaEs.get(pivote));


		while (comparacionI == -1) {
			i = i + 1;
			comparacionI = listaEs.get(i).compareTo(listaEs.get(pivote));
		}
		while (comparacionJ == 1) {
			j = j - 1;
			comparacionJ = listaEs.get(j).compareTo(listaEs.get(pivote));
		}
		if (i <= j ) {
			aux = listaEs.get(i);
			listaEs.set(i, listaEs.get(j));			
			listaEs.set(j, aux);
			i = i + 1;
			j = j-1;
		}
		if (izq < j) {
			quickSortEvaluacionEstudiantes(listaEs, izq, j);
		}
		if (i < der) {
			quickSortEvaluacionEstudiantes(listaEs, i, der);
		}
		return listaEs;

	}
	public void imprimir_notas (ArrayList<Estudiante> listaE) {
		System.out.println(listaE.size() + " elementos en la lista");
		for (Estudiante estudiante : listaE) {
			System.out.println(estudiante.getEvaluacion());
		}
	}

	public ArrayList<Trabajador> cargarTrabajadoresPrueba() {

		ArrayList<Trabajador> listaTr = new ArrayList();		
		for (int i = 0; i < 10; i++) {
			Trabajador est = new Trabajador(
					"nom"+i, 
					"ape"+i, 
					(Math.rint(Math.random()*1000000)/10),
					(Math.rint(Math.random()*1000)/10)
					);	
			listaTr.add(est);
			System.out.println(listaTr.get(i).toString());
		}		
		return listaTr;
	}
	public ArrayList<Trabajador> quickSortSalarioTr(ArrayList<Trabajador> listaTr, int izq, int der) {

		int pivote = (izq + der) / 2;
		Trabajador aux;
		int i = izq;
		int j = der;
		int comparacionI = listaTr.get(i).compareTo(listaTr.get(pivote));
		int comparacionJ = listaTr.get(j).compareTo(listaTr.get(pivote));


		while (comparacionI == -1) {
			i = i + 1;
			comparacionI = listaTr.get(i).compareTo(listaTr.get(pivote));
		}
		while (comparacionJ == 1) {
			j = j - 1;
			comparacionJ = listaTr.get(j).compareTo(listaTr.get(pivote));
		}
		if (i <= j ) {
			aux = listaTr.get(i);
			listaTr.set(i, listaTr.get(j));			
			listaTr.set(j, aux);
			i = i + 1;
			j = j-1;
		}
		if (izq < j) {
			quickSortSalarioTr(listaTr, izq, j);
		}
		if (i < der) {
			quickSortSalarioTr(listaTr, i, der);
		}
		return listaTr;

	}
	public void imprimir_salarios (ArrayList<Trabajador> lista) {
		System.out.println(lista.size() + " elementos en la lista");
		for (Trabajador T : lista) {
			System.out.println(T.getSalario());
		}
	}
	

}

