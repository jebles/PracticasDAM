package practica8;
import java.lang.Comparable;
import java.util.ArrayList;
/**
 * Estudiante.java		16 abr. 2019		 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * clase Estudiante, que derive de Persona, con un atributo llamado evaluacion.	
 */
public class Estudiante extends Persona implements Comparable <Estudiante> {
	
	private double evaluacion;

	public Estudiante(String nombre, String apellidos, double evaluacion) {
		super(nombre, apellidos);
		this.evaluacion = evaluacion;
	}
	public Estudiante() {
		new Persona();
		evaluacion = 0;
	}

	public double getEvaluacion() {
		return evaluacion;
	}

	public void setEvaluacion(double evaluacion) {
		this.evaluacion = evaluacion;
	}
	

	@Override
	public String toString() {
		return "Estudiante [evaluacion=" + evaluacion + 
				", nombre=" + nombre + 
				", apellidos=" + apellidos + "]"
				;
	}
	/**
	 * Ej 2, parte 1
	 * Metodo que compara las notas de evaluacion entre estudiantes
	 * @param Estudiante a comparar contra el estudiante actual
	 * @return 0 si son iguales, 1 si es mayor el actual y -1 si es menor
	 */
	@Override
	public int compareTo(Estudiante E) {
		if (this.evaluacion == E.evaluacion) {
			return 0;
		}
		if (this.evaluacion > E.evaluacion) {
			return 1;
		}
		else {
			return -1;
		}
	}

	

}
