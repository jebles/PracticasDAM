package practica8;

/**
 * Trabajador.java		16 abr. 2019		practica8 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * 
 * Definir una clase Trabajador, que derive Persona, con los atributos salario y
 * horasTrabajadas. Además dispone un método calcularSueldo(), que se calcula el salario, 
 * en base a a un precio y las horas trabajadas.
 */
public class Trabajador extends Persona implements Comparable<Trabajador> {
	
	private double salario;
	private double horasTrabajadas;
	
	
	public Trabajador(String nombre, String apellidos, double salario, double horasTrabajadas) {
		super(nombre, apellidos);
		this.salario = salario;
		this.horasTrabajadas = horasTrabajadas;
	}


	public double getSalario() {
		return salario;
	}


	public void setSalario(double salario) {
		this.salario = salario;
	}


	public double getHorasTrabajadas() {
		return horasTrabajadas;
	}


	public void setHorasTrabajadas(double horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}
	
	public double calcularSueldo(double horas, double precio) {
		return 0;
	}

	@Override
	public String toString() {
		return "Trabajador [salario=" + salario + 
				", horasTrabajadas=" + horasTrabajadas + 
				", nombre=" + nombre + 
				", apellidos=" + apellidos + "]"
				;
	}


	@Override
	public int compareTo(Trabajador o) {
		if (this.salario == o.salario) {
			return 0;
		}
		if (this.salario > o.salario) {
			return -1;
		}
		else {
			return 1;
		}
	}
	
	

}
