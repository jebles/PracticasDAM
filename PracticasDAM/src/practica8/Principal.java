package practica8;
import java.util.ArrayList;

/**
 * Principal.java		22 abr. 2019		practica8 
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * Pruebas de creación de objetos
 */
public class Principal {

	public static void main(String[] args) {
		
		Controlador  c = Controlador.getControlador();
//		ArrayList<Estudiante> listaRandom = c.cargarEstudiantesPrueba();	
//		c.imprimir_notas(listaRandom);
//
//		ArrayList<Estudiante> listaOrdenada = c.quickSortEvaluacionEstudiantes(listaRandom, 0, listaRandom.size()-1);
//		c.imprimir_notas(listaOrdenada);	
		
		ArrayList<Trabajador> listaRandom = c.cargarTrabajadoresPrueba();	
		c.imprimir_salarios(listaRandom);
		
		ArrayList<Trabajador> listaOrdenada = c.quickSortSalarioTr(listaRandom, 0, 9);
		c.imprimir_salarios(listaOrdenada);	
	}
}
