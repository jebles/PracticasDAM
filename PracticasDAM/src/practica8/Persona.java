package practica8;

/**
 * Punto6.java		22 abr. 2019		practica8
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * clase Persona con los atributos nombre y apellidos
 **/

//Una clase Persona con los atributos nombre y apellidos.
public class Persona {	

	public String nombre;
	public String apellidos;
	
	public Persona(String nombre, String apellidos) {
		this.nombre = nombre;
		this.apellidos = apellidos;
	}
	public Persona() {
		nombre = "nombre";
		apellidos = "apellidos";
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + 
				", apellidos=" + apellidos + "]"
				;
	}
	


}//Persona class


