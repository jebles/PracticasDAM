package practica6;


/**
 * ListaEnlazada.java		15 feb. 2019		practica6 - ejercicio 2
 * @author Jesús Pérez Robles  	NRE:4004010 DAM1
 * El método se llamará public void add(int indice, Object dato) sirve para poder insertar 
 * elementos en la estructura.
 */	

/**
 * Representa la implementación más sencilla y básica de una lista enlazada simple
 * con acceso sólo al principio de la serie de nodos.
 */
public class ListaEnlazada {

	public static void main(String[] args) {
		ListaEnlazada listaCompra = new ListaEnlazada();
		listaCompra.add("Leche");
		listaCompra.add("Miel");
		listaCompra.add("Aceitunas");
		listaCompra.add("Cerveza");
		listaCompra.add("Café");
		System.out.println("Lista de la compra:");
		for (int i = 0; i < listaCompra.size(); i++) {
			System.out.println(listaCompra.get(i));
		}
		System.out.println("elementos en la lista: " + listaCompra.size());
		System.out.println("elementos 3 en la lista: " + listaCompra.get(3));
		System.out.println("posición del elemento Miel: " + listaCompra.indexOf("Miel"));
		System.out.println("eliminado: " + listaCompra.remove("Miel"));
		System.out.println("Lista de la compra:");
		for (int i = 0; i < listaCompra.size(); i++) {
			System.out.println(listaCompra.get(i));
		}
		ListaEnlazada lista2 = new ListaEnlazada();
		lista2.add("Agua");
		lista2.add("Sal");
		listaCompra.add("Agua");
		listaCompra.add("Sal");
		
		System.out.println("Lista de la compra con agua y sal:");
		for (int i = 0; i < listaCompra.size(); i++) {
			System.out.println(listaCompra.get(i));
		}
		System.out.println("Eliminando el agua la la sal ");
		listaCompra.removeAll(lista2);
		System.out.println("Lista de la compra:");
		for (int i = 0; i < listaCompra.size(); i++) {
			System.out.println(listaCompra.get(i));
		}
	}

	// Atributos
	private Nodo primero;      // Referencia a nodo
	private int numElementos;
	private Nodo ultimo;
	/**
	 * Constructor que inicializa los atributos al valor por defecto.
	 * Lista vacía.
	 */
	public ListaEnlazada() {
		primero = null;
		numElementos = 0;
		ultimo = null;		
	}
	/** 
	 * Representa la estructura de un nodo para una lista dinámica con enlace simple.
	 */
	class Nodo {
		// Atributos
		Object dato;
		Nodo siguiente;    
		/**
		 * Constructor que inicializa atributos por defecto.
		 * @param elem - el elemento de información útil a almacenar.
		 */
		public Nodo(Object dato) {
			this.dato = dato;
			siguiente = null;
		}
		public Nodo() {
			dato = null;
			siguiente = null;
		}

	} // class

	// Métodos
	
	
	/**
	 * comprobar que el índice se encuentra dentro del rango de índices posibles que existen en la lista
	 * @param indice
	 * @return true si el indice introducido se sale del tamaño de la lista
	 */
	public boolean indiceInvalido ( int indice ) {
		return indice > numElementos || indice < 0;
	}
	/**
	 * Añade un objeto determinado en la posición indicada. Ejercicio verde 1º
	 * @param indice
	 * @param elem
	 * @author Jes
	 */
	public void add(int indice, Object elem) {
		assert elem != null;

		Nodo nuevo = new Nodo();

		nuevo.dato = elem;

		if (indiceInvalido(indice)) {
			throw new IndexOutOfBoundsException(indice); // arrojar un error de limite en caso que se produzca
		}			
		else if (numElementos == 0) {	//en caso de que la lista está vacía se añade auto al a primera posicion
			nuevo.siguiente = null;
			primero = nuevo;
			ultimo = nuevo;
			numElementos++;		

		}		
		else if (indice == numElementos){ 	//para el caso que se quiera añadir en la ultima posicion (creandola)
			ultimo.siguiente = nuevo; 
			ultimo = nuevo;
			nuevo.siguiente = null;
			numElementos++;		

		}
		else if ((indice == 0) && (numElementos > 0) ) { // caso de que se inserte en la primera posición			
			nuevo.siguiente = primero;
			primero = nuevo;
			numElementos++;

		}
		else { //insertar en la posición del indice 

			Nodo anterior = obtenerNodo(indice - 1);
			nuevo.siguiente = anterior.siguiente;
			anterior.siguiente = nuevo;
			numElementos++;		

		}		
	}
	/**
	 * eliminar de la lista todos los elementos que se le proporcionan en otra 
	 * lista pasada como argumento. Ejercicio verde 2
	 * @param Lista a borrar
	 */
	public void removeAll(ListaEnlazada datosAborrar) {
		
			for (int i = 0; i < datosAborrar.numElementos; i++) {				
				for (int j = 0; j < this.numElementos; j++) {
					if (datosAborrar.get(i).equals(this.get(j))) {
						this.remove(j);				
						}					
				}					
			}						
	}
	/**
	 * Añade un elemento al final de la lista.
	 * @param elem - el elemento a añadir.
	 * Admite que el elemento a añadir sea null.
	 */
	public void add(Object dato) {     
		//variables auxiliares
		Nodo nuevo = new Nodo(dato);
		Nodo ultimo = null;
		if (numElementos == 0) {
			// Si la lista está vacía enlaza el nuevo nodo el primero.
			primero = nuevo;
		}
		else {
			// Obtiene el último nodo y enlaza el nuevo.
			ultimo = obtenerNodo(numElementos-1);
			ultimo.siguiente = nuevo;
		}
		numElementos++;               // Actualiza el número de elementos.
	}

	/**
	 * Obtiene el nodo correspondiente al índice. Recorre secuencialmente la cadena de enlaces. 
	 * @param indice - posición del nodo a obtener.
	 * @return - el nodo que ocupa la posición indicada por el índice.
	 * @exception IndexOutOfBoundsException - índice no está entre 0 y numElementos-1
	 */
	private Nodo obtenerNodo(int indice) {
		// Lanza excepción si el índice no es válido.
		if (indice >= numElementos || indice < 0) {
			throw new IndexOutOfBoundsException("Índice incorrecto: " + indice);
		}
		// Recorre la lista hasta llegar al nodo  de posición buscada.
		Nodo actual = primero;
		for (int i = 0; i < indice; i++)
			actual = actual.siguiente;
		return actual;
	}
	/**
	 * Elimina el elemento indicado por el índice. Ignora índices negativos
	 * @param indice - posición del elemento a eliminar
	 * @return - el elemento eliminado o null si la lista está vacía.
	 * @exception IndexOutOfBoundsException - índice no está entre 0 y numElementos-1
	 */
	public Object remove(int indice) {
		// Lanza excepción si el índice no es válido.
		if (indice >= numElementos || indice < 0) {
			throw new IndexOutOfBoundsException("Índice incorrecto: " + indice);
		}        
		if (indice > 0) {        
			return removeIntermedio(indice);
		}        
		if (indice == 0) {
			return removePrimero();
		}        
		return null;
	}

	/**
	 * Elimina el primer elemento.
	 * @return - el elemento eliminado o null si la lista está vacía.
	 */
	private Object removePrimero() {
		//variables auxiliares
		Nodo actual = null;
		actual = primero;               // Guarda actual.
		primero = primero.siguiente;       // Elimina elemento del principio.
		numElementos--;
		return actual.dato;
	}

	/**
	 * Elimina el elemento indicado por el índice. 
	 * @param indice - posición del elemento a eliminar.
	 * @return - el elemento eliminado o null si la lista está vacía.
	 */
	private Object removeIntermedio(int indice) {
		//variables auxiliares
		Nodo actual = null;
		Nodo anterior = null;
		// Busca nodo del elemento anterior correspondiente al índice.
		anterior = obtenerNodo(indice - 1);
		actual = anterior.siguiente;             // Guarda actual.
		anterior.siguiente = actual.siguiente;      // Elimina el elemento.
		numElementos--;
		return actual.dato;    
	}
	/**
	 * Elimina el dato especificado.
	 * @param dato – a eliminar.
	 * @return - el índice del elemento eliminado o -1 si no existe.
	 */
	public int remove(Object dato) { 
		// Obtiene el índice del elemento especificado.
		int actual = indexOf(dato);
		if (actual != -1) {
			remove(actual);        // Elimina por índice.
		}
		return actual;
	}

	/**
	 * Busca el índice que corresponde a un elemento de la lista.
	 * @param dato- el objeto elemento a buscar.
	 */
	public int indexOf(Object dato) {
		Nodo actual = primero;
		for (int i = 0; actual != null; i++) {
			if ((actual.dato != null && actual.dato.equals(dato))
					|| actual.dato == dato) {
				return i;
			}
			actual = actual.siguiente;
		}
		return -1;
	}
	/**
	 * @param indice – obtiene un elemento por su índice.
	 * @return elemento contenido en el nodo indicado por el índice.
	 * @exception IndexOutOfBoundsException - índice no está entre 0 y numElementos-1.
	 */
	public Object get(int indice) {
		// lanza excepción si el índice no es válido
		if (indice >= numElementos || indice < 0) {
			throw new IndexOutOfBoundsException("índice incorrecto: " + indice);
		}
		Nodo aux = obtenerNodo(indice);
		return aux.dato;
	} 

	/**
	 * @return el número de elementos de la lista
	 */
	public int size() {
		return numElementos;
	}


} // class





